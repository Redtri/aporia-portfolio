﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.VFX;

public class DustManager : MonoBehaviour
{
    public Object[] spicyDusts;
    public Transform pawn;
    public float minDist;
    
    // Start is called before the first frame update
    void Start()
    {
        spicyDusts = Object.FindObjectsOfType(typeof(VisualEffect));
    }

    // Update is called once per frame
    void Update()
    {
        foreach (VisualEffect effect in spicyDusts)
        {
            effect.SetFloat("Disturbance", 1f-(Vector3.Distance(pawn.position, effect.transform.position)/minDist));
            //effect.SetInt("Amount", (int)((1f-(Vector3.Distance(pawn.position, effect.transform.position)/minDist)) * 30f));
        }
        Debug.DrawLine(pawn.transform.position, pawn.transform.position + pawn.transform.forward * minDist, Color.green);
        Debug.DrawLine(pawn.transform.position, pawn.transform.position - pawn.transform.forward * minDist, Color.green);
        Debug.DrawLine(pawn.transform.position, pawn.transform.position - pawn.transform.right * minDist, Color.green);
        Debug.DrawLine(pawn.transform.position, pawn.transform.position + pawn.transform.right * minDist, Color.green);
    }
}
