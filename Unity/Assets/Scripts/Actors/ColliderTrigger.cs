﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class ColliderTrigger : MonoBehaviour
{
    public List<string> tags;
    public UnityEvent onTriggerEnt;
    public UnityEvent onTriggerEx;
    public int nbMax;
    private int currentNb;
    public bool active = true;

    private void Start()
    {
    }

    public void Activate()
    {
        active = true;
    }

    public void Disable()
    {
        active = false;
    }
    
    private void OnTriggerEnter(Collider other)
    {
        if (active && (nbMax == 0 || currentNb < nbMax))
        {
            //Debug.Log(other.tag);
            if (tags.Contains(other.tag))
            {
                onTriggerEnt?.Invoke();
                if (nbMax > 0)
                    ++currentNb;
            }
        }

    }

    private void OnTriggerExit(Collider other)
    {
        if (active)
        {
            //Debug.Log(other.tag);
            if(tags.Contains(other.tag))
                onTriggerEx?.Invoke();
        }
    }
}
