﻿using System;
using System.Collections;
using System.Collections.Generic;
using DG.Tweening;
using UnityEngine;
using UnityEngine.Events;

public class TranslateActor : MonoBehaviour
{
    public Transform spot;
    public float duration;
    public AnimationCurve curve;
    private Vector3 destCheckedOut;
    public bool checkoutDestOnStart;
    public UnityEvent onEnd;

    private void Start()
    {
        if (checkoutDestOnStart)
            destCheckedOut = spot.transform.position;
    }

    public void Translate()
    {
        Vector3 position = (checkoutDestOnStart) ? destCheckedOut : spot.transform.position;
        //DOVirtual.Float(0f, 1f, duration, value => () transform.position = )
        transform.DOMove(position, duration).SetEase(curve).onComplete += () => onEnd?.Invoke();
    }
}
