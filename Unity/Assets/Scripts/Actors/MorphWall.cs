﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.Events;

public class MorphWall : MonoBehaviour
{
    public RoomTileset roomTileset;
    public int newWallIndex;
    public Wall targetWall;
    [HideInInspector] public bool hasMorphed;
    [Header("Extra door parameters")]
    public GameObject extraDoor;

    public AK.Wwise.Event morphEvent;

    public bool singleTry;
    public InteractEvent tryInteractEvent;
    public Door2Event passThroughEvent;
    public InteractEvent finishedEvent;
    public eDOOR_TRAVELMODE travelMode;
    public bool autotravel;
    public bool locked;
    public Vector3 colliderExtent;
    public Vector3 colliderOffset;

    public void ChangeWallMorph()
    {
        WallHandler handler = targetWall.handler;
        string typeName = roomTileset.wallTileset[newWallIndex].prefab.GetComponent<Wall>().typeName;
        
        targetWall = handler.ChangeWallType(handler.GetWall(targetWall.gameObject), typeName);

        Door doorAttempt = null;
        if (targetWall.TryGetComponent(out doorAttempt))
        {
            morphEvent?.Post(targetWall.gameObject);
            doorAttempt.locked = locked;
            doorAttempt.travelMode = travelMode;
            
            Portal portalAttempt = doorAttempt.GetComponent<Portal>();
            if (doorAttempt.travelMode != eDOOR_TRAVELMODE.SimpleSelf && doorAttempt.travelMode != eDOOR_TRAVELMODE.SimpleToNextRoom && portalAttempt) {
                if(extraDoor)
                    portalAttempt.otherPortal = extraDoor.GetComponent<Portal>();
                portalAttempt.flipCam = true;
                if (colliderExtent != Vector3.zero)
                {
                    BoxCollider collider = portalAttempt.GetComponents<BoxCollider>().ToList().Find(arg0 => arg0.isTrigger);
                    collider.size = colliderExtent;
                    if (colliderOffset != Vector3.zero)
                        collider.center = colliderOffset;
                }
            }

            doorAttempt.autotravel = autotravel;
            doorAttempt.onTryInteract.AddListener(tryInteractEvent.Invoke);
            if(singleTry)
                doorAttempt.onTryInteract.AddListener(arg0 => doorAttempt.ActivateOnTry(false));
            
            //doorAttempt.onPortalSend.AddListener(passThroughEvent.Invoke);
            doorAttempt.onFinished.AddListener(finishedEvent.Invoke);
            targetWall.generator.GetComponent<DoorManager>().AddDoor(doorAttempt);
            //A60B00
        }
    }
}
