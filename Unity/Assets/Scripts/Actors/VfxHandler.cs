﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.VFX;

public class VfxHandler : MonoBehaviour
{
    public List<VisualEffect> vfxObj;

    public void Emit()
    {
        foreach (VisualEffect vfx in vfxObj)
        {
            vfx.SendEvent("Play");
        }
    }

    public void Stop()
    {
        foreach (VisualEffect vfx in vfxObj)
        {
            vfx.Stop();
        }
    }
}
