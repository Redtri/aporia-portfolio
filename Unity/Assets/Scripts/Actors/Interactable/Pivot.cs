﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

public class Pivot : Interactable
{
    public Vector3 finalRotation;
    private Vector3 startRotation;
    public float duration;
    public AnimationCurve curve;
    private bool rotated;

    private void Start()
    {
        startRotation = transform.localRotation.eulerAngles;
    }
    
    public override bool Interact(Pawn pawnUser, eINTERACT_TYPE interactType, bool start = true)
    {
        if ((interactType & useModality) != eINTERACT_TYPE.None)
        {
            onTryInteract?.Invoke(this);
            tryInteractEvent?.Post(gameObject);
            if (maxUse > 0) {
                if (nbUse < maxUse) {
                    Rotate();
                    onSuccesInteract?.Invoke(this);
                    successInteractEvent?.Post(gameObject);
                    ++nbUse;
                    return true;
                }
                onFailInteract?.Invoke(this);
                failInteractEvent?.Post(gameObject);
                return false;
            }
            Rotate();
            onSuccesInteract?.Invoke(this);
            successInteractEvent?.Post(gameObject);
            return true;
        }

        return false;
    }

    private void Rotate()
    {
        if (rotated)
        {
            transform.DOLocalRotate(startRotation, duration).SetEase(curve);
        }else {
            transform.DOLocalRotate(finalRotation, duration).SetEase(curve);
        }
        rotated = !rotated;
    }
}
