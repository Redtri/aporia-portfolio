﻿using System;
using System.Collections;
using System.Collections.Generic;
using AK.Wwise;
using UnityEngine;
using DG.Tweening;
using UnityEngine.PlayerLoop;
using UnityEngine.VFX;

public enum eDOOR_STATE{CLOSED, OPENING, OPEN, CLOSING}
public enum eDOOR_SIDE{Right, Left}
public enum eDOOR_TRAVELMODE{SimpleSelf, SimpleToNextRoom, PortalToOtherPortal, PortalToOtherRoom, PortalToNextRoom}

public class Door : Interactable
{
    public Transform doorParent;
    [Header("Parameters")]
    public float duration;
    public eDOOR_SIDE doorSide;
    public AnimationCurve animCurve;
    public EffectProfile shakeProfile;
    public EffectProfile glitchProfile;
    public EffectProfile noGlitchProfile;
    [Header("Travel options")]
    public bool overrideLoop = true;
    public eDOOR_TRAVELMODE travelMode;
    [Header("State options")]
    public bool locked;
    public bool trapped;
    
    public bool autoClose;
    
    public Door replicateDoor;
    public DoorManager otherRoom;
    [Header("DEBUG")]
    public bool autotravel;

    [Header("References")] 
    public Transform entrance;
    public Transform exit;
    public DoorManager doorManager;
    public Portal portal;
    public eDOOR_STATE doorState { get; private set; }
    public bool isModel { get; private set; }
    public InteractEvent onFinished;
    public AK.Wwise.Event finishedEvent;
    public AK.Wwise.Event onTrapped;
    public AK.Wwise.Event knock;
    public Door2Event onPortalSend;
    public Door2Event onPortalReceive;
    [HideInInspector] public bool used;

    private void Awake()
    {
        if (TryGetComponent(out portal))
        {
            portal.onSend.AddListener(OnPortalSend);
            portal.onReceive.AddListener(OnPortalReceive);
        }
    }

    private void Start()
    {
        Init();
    }

    private void Init()
    {
        doorState = eDOOR_STATE.CLOSED;
        SetSide(doorSide);
        isModel = false;
        used = false;
        user = GameObject.Find("Pawn").GetComponent<Pawn>();
    }

    public void SetSide(eDOOR_SIDE newSide)
    {
        doorSide = newSide;
        switch (doorSide)
        {
            case eDOOR_SIDE.Left:
                if (doorParent)
                {
                    doorParent.localPosition = new Vector3(-0.4067802f, -0.3690157f, 0.0001112088f);
                    doorParent.localRotation = Quaternion.Euler(new Vector3(0, 180, 0));
                }
                break;
            case eDOOR_SIDE.Right:
                if (doorParent)
                {
                    doorParent.localPosition = new Vector3(0.4140643f, -0.3690157f, 0.008497662f);
                    doorParent.localRotation = Quaternion.Euler(new Vector3(0, 0, 0));
                }
                break;
        }
    }

    public void Knock(bool start)
    {
        if(start)
            knock?.Post(gameObject);
        else
            knock?.Stop(gameObject);
    }

    public void ExceptionOpen()
    {
        if (doorState == eDOOR_STATE.CLOSED)
        {
            isModel = true;
            TriggerDoor(Pawn.controlledPawn);
        }
    }
    
    /*
    private void Update()
    {
        Debug.DrawLine(transform.position, transform.position + transform.up * 10f, Color.green);
        Debug.DrawLine(transform.position, transform.position + transform.right * 10f, Color.red);
        Debug.DrawLine(transform.position, transform.position + transform.forward * 10f, Color.blue);
        Debug.DrawLine(user.transform.position, user.transform.position + user.transform.up * 10f, Color.green);
        Debug.DrawLine(user.transform.position, user.transform.position + user.transform.right * 10f, Color.red);
        Debug.DrawLine(user.transform.position, user.transform.position + user.transform.forward * 10f, Color.blue);
        
    }
    */

    public void Replicate(Door model)
    {
        //Debug.Log(name + " trying to replicate " + model.name + ". Being " + ((isModel) ? " a model." : " a replicator."));
        if (!isModel) {
            DOTween.Kill(doorParent);
            doorState = model.doorState;
            duration = model.duration;
            TriggerDoor(model.user);
        }
    }

    public void OnPortalSend(Portal emitter, Portal receiver)
    {
        if (portal.otherPortal)
            onPortalSend?.Invoke(this, portal.otherPortal.GetComponent<Door>());
        else
            onPortalSend?.Invoke(this, null);
    }
    
    public void OnPortalReceive(Portal receiver, Portal emitter)
    {
        onPortalReceive?.Invoke(this, portal.otherPortal.GetComponent<Door>());
    }
    
    //Interact is called from the pawn, it sets the door as a model to call replicated doors, it also avoid duplicata Wwise events
    public override bool Interact(Pawn pawnUser, eINTERACT_TYPE interactType, bool start = true)
    {
        //Trying to interact
        if(onTryEnabled)
            onTryInteract?.Invoke(this);
        tryInteractEvent?.Post(gameObject);

        if ((interactType & useModality) != eINTERACT_TYPE.None)
        {
            if (doorState == eDOOR_STATE.OPEN || doorState == eDOOR_STATE.CLOSED)
            {
                if (locked) {
                    if(onFailEnabled)
                        onFailInteract?.Invoke(this);
                    failInteractEvent?.Post(gameObject);

                    return false;
                }
                if(onSuccesEnabled)
                    onSuccesInteract?.Invoke(this);
                successInteractEvent?.Post(gameObject);
            
                isModel = true;
                TriggerDoor(pawnUser);
            
                return true;
            }
        }

        return false;
    }

    //TriggerDoor can be called from either Interact or from a model door
    private bool TriggerDoor(Pawn pawnUser)
    {
        if(!used)
            used = true;
        
        switch (doorState)
        {
            case eDOOR_STATE.CLOSED:
                user = pawnUser;
                switch (doorSide)
                {
                    case eDOOR_SIDE.Left:
                        doorParent.DOLocalRotate(new Vector3(0f, (!isModel) ? -90f : 90f, 0f), duration*2).SetEase(animCurve).OnComplete(() => DoorFinished());
                        break;
                    case eDOOR_SIDE.Right:
                        doorParent.DOLocalRotate(new Vector3(0f, (isModel) ? 90f : -90f, 0f), duration*2).SetEase(animCurve).OnComplete(() => DoorFinished());
                        break;
                }
                
                if (isModel)
                {
                    if (autotravel)
                    {
                        Sequence moveSeq = DOTween.Sequence();
                        //Debug.Log(entrance.position + " " + entrance.localPosition);
                        //moveSeq.Append(user.transform.DOMove(new Vector3(entrance.position.x, user.transform.position.y, entrance.position.z), 0.5f));
                        float delta = Vector3.SignedAngle(user.transform.forward, transform.forward, user.transform.up);

                        //moveSeq.Join(user.transform.DORotate(new Vector3(0f, user.transform.rotation.eulerAngles.y + delta, 0f), 1f));
                        user.BlockPawn();
                        moveSeq.Join(user.cam.transform.DOLocalRotate(new Vector3(0f, 0f, 0f), 1f));
                        if (trapped) {
                            moveSeq.AppendCallback(() => user.PushBack(duration*2f,(user.transform.position - transform.position) * 500f, noGlitchProfile));
                            onTrapped?.Post(gameObject);
                            EffectManager.instance.ScreenShake(shakeProfile);
                            PostProcessManager.instance.Effect(glitchProfile);
                        } else {
                            moveSeq.AppendCallback(() => user.MoveForward(duration, Vector3.Distance(entrance.position, exit.position)));
                        }
                    }
                    if (replicateDoor != null)
                    {
                        //Debug.Log("Effectively replicate door");
                        replicateDoor.GetComponent<Portal>().portalScreen.gameObject.SetActive(true);
                        //SetActive(false);
                        //replicateDoor.SetActive(true);
                        replicateDoor.Replicate(this);
                        if (isModel)
                            replicateDoor.GetComponent<Portal>().portalScreen.gameObject.SetActive(true);
                    }
                    else
                    {
                        //Debug.Log("No replicate door");
                    }
                }
                doorState = eDOOR_STATE.OPENING;
                return true;
            case eDOOR_STATE.OPEN:
                user = pawnUser;
                switch (doorSide)
                {
                    case eDOOR_SIDE.Left:
                        doorParent.DOLocalRotate(new Vector3(0f, 180f, 0f), duration).SetEase(animCurve).OnComplete(() => DoorFinished());
                        break;
                    case eDOOR_SIDE.Right:
                        doorParent.DOLocalRotate(new Vector3(0f, 0f, 0f), duration).SetEase(animCurve).OnComplete(() => DoorFinished());
                        break;
                }
                if (isModel)
                {
                    if(replicateDoor != null)
                        replicateDoor.Replicate(this);
                }
                doorState = eDOOR_STATE.CLOSING;
                return true;
        }

        return false;
    }

    public void Reactive(float duration)
    {
        StartCoroutine(Activation(duration));
    }

    private IEnumerator Activation(float duration)
    {
        doorParent.gameObject.SetActive(false);
        yield return new WaitForSeconds(duration);
        doorParent.gameObject.SetActive(true);
        yield return null;
    }

    public void SetActive(bool show)
    {
        doorParent.gameObject.SetActive(show);
    }

    public void AssignWithNextPortal(Portal newPortal)
    {
        //Debug.Log("Assign " + newPortal);
        travelMode = eDOOR_TRAVELMODE.PortalToOtherPortal;
        portal.otherPortal = newPortal;
    }

    public void Void(bool willOverrideLoop)
    {
        if (portal)
        {
            portal.portalType = ePORTAL.Void;
            portal.RefreshType();
        }
        overrideLoop = willOverrideLoop;
    }

    public void Standard(bool willOverrideLoop)
    {
        if (portal)
        {
            portal.portalType = ePORTAL.Standard;
            portal.RefreshType();
        }
        doorParent.gameObject.SetActive(true);
        overrideLoop = willOverrideLoop;
    }

    public void ForceClose(Pawn pawn)
    {
        doorParent.DOKill();
        doorState = eDOOR_STATE.OPEN;
        //float oldDuration = duration;
        duration = 0.1f;
        TriggerDoor(pawn);
        StartCoroutine(ResetDuration(2f));
    }

    private IEnumerator ResetDuration(float oldDuration)
    {
        yield return new WaitForSeconds(duration);
        duration = oldDuration;
        yield return null;
    }
    
    private void DoorFinished()
    {
        if (autoClose)
        {
            switch (doorState)
            {
                case eDOOR_STATE.OPENING:
                    doorState = eDOOR_STATE.OPEN;
                    TriggerDoor(user);
                    break;
                
                case eDOOR_STATE.CLOSING:
                    doorState = eDOOR_STATE.CLOSED;
                    if(isModel)
                        finishedEvent.Post(gameObject);
                    onFinished?.Invoke(this);
                    ResetModelState();
                    break;
            }
        } else {
            switch (doorState)
            {
                case eDOOR_STATE.OPENING:
                    doorState = eDOOR_STATE.OPEN;
                    onFinished?.Invoke(this);
                    ResetModelState();
                    break;
                
                case eDOOR_STATE.CLOSING:
                    doorState = eDOOR_STATE.CLOSED;
                    onFinished?.Invoke(this);
                    ResetModelState();
                    break;
            }
        }
    }

    private void ResetModelState()
    {
        if (isModel) {
            isModel = false;
            if (doorManager.roomManager.roomIndex == GameManager.instance.currRoomIndex)
                SetActive(true);
        }
    }

    public void Lock(bool unlock = false)
    {
        locked = !unlock;
    }
}
