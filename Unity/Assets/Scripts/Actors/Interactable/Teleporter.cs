﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;
using UnityEngine.PlayerLoop;

public class Teleporter : MonoBehaviour
{
    public Transform target;
    public Teleporter destTeleporter;
    public Transform entity;
    private Vector3 offset;
    private Vector3 startPos;
    public RoomManager parent;

    public bool checkedOut;

    private void Awake()
    {
        if (destTeleporter)
        {
            
            offset = destTeleporter.target.position - target.position;
            startPos = target.transform.position;
        }
    }

    public void Update()
    {
        if (checkedOut)
        {
            target.position = entity.position;
            destTeleporter.target.position = target.position + offset;
        }
    }
    
    public void Checkout()
    {
        ResetTarget();
        checkedOut = true;
    }

    public void Teleport()
    {
        //Debug.Log("ME AS : " + parent.roomIndex + ". Destination teleporter " + destTeleporter.parent.roomIndex + " " + destTeleporter.parent.name);
        int roomIndex = destTeleporter.parent.roomIndex;
        GameManager.instance.AskForRoom(roomIndex);
        checkedOut = false;
        entity.position = destTeleporter.target.position;
        GameManager.instance.rooms[roomIndex].TeleportInto();
        ResetTarget();
    }

    public void ResetTarget()
    {
        target.position = startPos;
    }
}
