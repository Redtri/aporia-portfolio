﻿using System;
using System.Collections;
using System.Collections.Generic;
using DG.Tweening;
#if UNITY_EDITOR
using UnityEditor;
#endif
using UnityEngine;

public class RoomGenerator : MonoBehaviour
{

    //public RoomInfos roomInfos;
    public RoomTileset roomTileset;
    public Transform roomCenter;
    [Range(0, 20)] public int startNbWalls;
    [HideInInspector] public int previousNbWalls;
    [Range(1, 20)] public int height;
    [HideInInspector] public int previousHeight;
    //GROUND
    public bool hasGround = true;
    private bool prevHasGround;
    public GameObject groundTile;
    public SurfaceHandler floorHandler;
    public GameObject roofTile;
    public SurfaceHandler roofHandler;
    //WALLS
    public GameObject wallTile;
    public WallHandler nort;
    public WallHandler east;
    public WallHandler south;
    public WallHandler west;
    //ROOF
    public bool hasRoof = true;
    private bool prevHasRoof;
    //FURNITURES
    public Transform furnitureParent;
    public List<FurnitureInfos> furnitures;
    private bool started = false;

    private void Start()
    {
        started = true;
        CheckValidity();
    }

    private void OnDisable()
    {
        started = false;
    }

    public void CheckValidity()
    {
        #if UNITY_EDITOR
        if (!EditorApplication.isPlayingOrWillChangePlaymode || started)
        {
        #endif
            if (height > 0)
            {
                if (startNbWalls > 0)
                {
                    bool obsolete = false;
                    //Checking if the room has been generated and references are binded
                    //Rebinding references
                    WallHandler[] wallHandlers = new[] {nort, east, south, west};
                    for (int i = 0; i < wallHandlers.Length; ++i)
                    {
                        if (!wallHandlers[i].IsValid() && wallHandlers[i].parent.childCount > 0)
                        {
                            obsolete = false;
                            //Debug.Log("Initializing " + wallHandlers[i].parent.name);
                            wallHandlers[i].Init(this, roomTileset, roomCenter, startNbWalls, height);
                            //Debug.Log(wallHandlers[i].nbWalls + " " + wallHandlers[i].walls.Count);
                            if(wallHandlers[i].CanRebind()){
                                wallHandlers[i].RebindWalls(out obsolete);
                                if (obsolete)
                                {
                                    previousHeight = height = 1;
                                }
                            }
                        }
                    }

                    if (hasGround) {
                        if (!floorHandler.IsValid() && floorHandler.parent.childCount > 0) {
                            floorHandler.Init(this, roomCenter, roomTileset, wallHandlers[0], wallHandlers[2], Utils.instance.GetTileDimensions(roomTileset.GetDefault(eROOM_PART.Floor).prefab));
                            floorHandler.Rebind();
                        }
                    }else{
                        floorHandler.Destroy();
                    }
        
                    if (hasRoof) {
                        if (!roofHandler.IsValid() && roofHandler.parent.childCount > 0) {
                            roofHandler.Init(this, roomCenter, roomTileset, wallHandlers[0], wallHandlers[2], Utils.instance.GetTileDimensions(roomTileset.GetDefault(eROOM_PART.Roof).prefab));
                            roofHandler.Rebind();
                        }
                    } else {
                        roofHandler.Destroy(); 
                    }
                }
                //Generating or regenerating the room if a consistent value changed
                if (startNbWalls != previousNbWalls || height != previousHeight || prevHasGround != hasGround || prevHasRoof != hasRoof){
                    //Debug.Log("Regenerating startWall : " + startNbWalls + " previousWall : " + previousNbWalls + " height : " + height + " prevHeight :"+ previousHeight);
                    GenerateRoom();
                }
            }
        #if UNITY_EDITOR
        }
        #endif
    }
    
    public void GenerateRoom()
    {
        WallHandler[] wallHandlers = new[] {nort, east, south, west};
        //Iterating over as many wall handlers as there are
        for (int i = 0; i < wallHandlers.Length; ++i) {
            wallHandlers[i].Init(this, roomTileset, roomCenter, startNbWalls, height);
            //wallHandlers[i].RebindWalls();
            wallHandlers[i].GenerateWalls();
        }

        if (hasGround) {
            floorHandler.Init(this, roomCenter, roomTileset, wallHandlers[0],wallHandlers[2], Utils.instance.GetTileDimensions(roomTileset.GetDefault(eROOM_PART.Wall).prefab));
            //floorHandler.Rebind();
            floorHandler.Generate();
        } else {
            floorHandler.Destroy();
        }
        
        
        if (hasRoof) {
            roofHandler.Init(this, roomCenter, roomTileset, wallHandlers[0],wallHandlers[2], Utils.instance.GetTileDimensions(roomTileset.GetDefault(eROOM_PART.Wall).prefab));
            //floorHandler.Rebind();
            roofHandler.Generate();
        } else {
            roofHandler.Destroy();
        }

    
        BindFurnitures();
        previousNbWalls = startNbWalls;
        previousHeight = height;
        prevHasRoof = hasRoof;
        prevHasGround = hasGround;
    }

    private void OnValidate()
    {
        CheckValidity();
    }

    public void Clear()
    {
        for (int i = 0; i < Enum.GetValues(typeof(WallHandler.eCAPE)).Length; ++i)
        {
            WallHandler[] wallHandlers = new[] {nort, east, south, west};
            foreach (Transform child in wallHandlers[i].parent) {
                if (wallHandlers[i].walls != null)
                {
                    if(!wallHandlers[i].Contains(child.gameObject))
                        Utils.instance.Destroyasse(child.gameObject);
                }
            }
        }
        
        foreach (Transform child in floorHandler.parent) {
            if (floorHandler.tiles != null)
            {
                if(floorHandler.tiles.Find(x => x.gameObject == child.gameObject) != null)
                    Utils.instance.Destroyasse(child.gameObject);
            }
        }
        
        foreach (Transform child in roofHandler.parent) {
            if (roofHandler.tiles != null)
            {
                if(roofHandler.tiles.Find(x => x.gameObject == child.gameObject) != null)
                    Utils.instance.Destroyasse(child.gameObject);
            }
        }
    }

    public void Rebind()
    {
        bool obsolete;
        WallHandler[] wallHandlers = new[] {nort, east, south, west};
        for (int i = 0; i < wallHandlers.Length; ++i)
        {
            wallHandlers[i].RebindWalls(out obsolete);
        }
        floorHandler.Rebind();
        roofHandler.Rebind();
        BindFurnitures();
    }

    public void RefreshMaterials()
    {
        CheckValidity();
        WallHandler[] wallHandlers = new[] {nort, east, south, west};
        for (int i = 0; i < wallHandlers.Length; ++i)
        {
            wallHandlers[i].roomTileset = roomTileset;
            wallHandlers[i].RefreshWalls();
        }
        floorHandler.RefreshSurfaces();
        roofHandler.RefreshSurfaces();
    }
    
    private void BindFurnitures()
    {
        furnitures = new List<FurnitureInfos>();
        
        foreach (Transform child in furnitureParent)
        {
            FurnitureInfos newFurniture = new FurnitureInfos(child.gameObject, new Vector3(roomCenter.position.x, furnitureParent.position.y, roomCenter.position.z));
            furnitures.Add(newFurniture);
        }
    }


    public WallHandler GetHandler(Transform trsf)
    {
        WallHandler[] wallHandlers = new[] {nort, east, south, west};
        for (int i = 0; i < wallHandlers.Length; ++i)
        {
            if (trsf == wallHandlers[i].parent)
                return wallHandlers[i];
        }

        return null;
    }
}
