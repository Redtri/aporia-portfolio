﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class CheckoutListener : MonoBehaviour
{
    public int startListenOver;
    private RoomManager roomManager;
    public UnityEvent onCheckout;
    private int count;

    // Start is called before the first frame update
    void Start()
    {
    }

    private void OnEnable()
    {
        roomManager = GetComponent<RoomManager>();
        roomManager.onCheckout.AddListener(OnCheckout);
    }

    private void OnDisable()
    {
        roomManager.onCheckout.RemoveListener(OnCheckout);
    }

    private void OnCheckout()
    {
        ++count;
        if (count > startListenOver)
        {
            onCheckout?.Invoke();
        }
    }
}
