﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Wall : MonoBehaviour
{
    public string typeName;
    [HideInInspector] public RoomGenerator generator;
    [HideInInspector] public WallHandler handler;
    public List<MeshRenderer> renderers;
    public int floor;
    public void Autoname()
    {
        //Debug.Log("Autonaming wall");
        //typeName = generator.roomTileset.GetDefault(eROOM_PART.Wall).typeName;
    }
}
