﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Surface : MonoBehaviour
{
    public string typeName;
    public RoomGenerator generator;
    public SurfaceHandler handler;
    public List<MeshRenderer> renderers;
    public void Autoname()
    {
        //Debug.Log("Autonaming surface");
        //typeName = generator.roomTileset.GetDefault(handler.roomPart).typeName;
    }
}
