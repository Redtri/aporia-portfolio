﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

public class Painting : Wall
{
    [HideInInspector] public Texture text;
    public Material paintMat;

    public MeshRenderer meshRenderer;

    private void OnValidate()
    {
        /*
        if (meshRenderer != null && paintMat != null)
        {
            meshRenderer.material = paintMat;
        }
        */
    }

    public void DissolveFade(float duration)
    {
        meshRenderer.material.DOFloat(0f, "Alpha", duration);
    }
}
