﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DealingTeleporter : MonoBehaviour
{
    public Vector2 delayRange;
    public Transform target;
    public List<Transform> spots;

    private bool running;
    private float currentDelay;
    private Transform currentSpot;

    public void Trigger()
    {
        if (!running) {
            running = true;
            currentSpot = spots[0];
            StartCoroutine(TeleportCo());
        } else {
            running = false;
            StopAllCoroutines();
        }
    }

    private IEnumerator TeleportCo()
    {
        currentDelay = Random.Range(delayRange.x, delayRange.y);
        int randomIndex = Random.Range(0, spots.Count);

        while (spots.IndexOf(currentSpot) == randomIndex && spots.Count > 1)
            randomIndex = Random.Range(0, spots.Count);

        currentSpot = spots[randomIndex];
        target.position = currentSpot.position;

        yield return new WaitForSeconds(currentDelay);

        if (running) {
            StartCoroutine(TeleportCo());
            yield return null;
        } else {
            StopAllCoroutines();
            yield return null;
        }
    }
}
