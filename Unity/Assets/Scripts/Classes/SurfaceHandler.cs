﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
#if UNITY_EDITOR
using UnityEditor;
#endif

[System.Serializable]
public class SurfaceHandler
{
    [System.Serializable]
    public class SurfaceInfos
    {
        public GameObject gameObject;
        
        public SurfaceInfos(GameObject go, RoomGenerator generator, SurfaceHandler handler)
        {
            gameObject = go;
            gameObject.GetComponent<Surface>().generator = generator;
            gameObject.GetComponent<Surface>().handler = handler;
        }

        public void Reset(GameObject go, RoomGenerator generator, SurfaceHandler handler)
        {
            gameObject = go;
            gameObject.GetComponent<Surface>().generator = generator;
            gameObject.GetComponent<Surface>().handler = handler;
        }
    }
    [HideInInspector]public RoomTileset roomTileset;
    
    public bool isFloor;
    public Transform parent;
    private Transform roomCenter;
    //Space unit between ground tiles
    [HideInInspector] public Vector3 dimensions;
    [HideInInspector] public Vector3 wallDimensions;
    [HideInInspector] public Vector3 roomDimensions;
    private Vector3 roomStart;
    private Vector3 roomEnd;
    [HideInInspector] public List<SurfaceInfos> tiles;
    private WallHandler srcHandler;
    private WallHandler dstHandler;
    private int tilesPerRow;
    private int prevTilesPerRow;
    private Vector3 oppositeDir;
    [HideInInspector] public eROOM_PART roomPart;
    private RoomGenerator owner;
    
    [HideInInspector] public bool initialized = false;

    public void Init(RoomGenerator generator, Transform center, RoomTileset newRoomTileset, WallHandler src, WallHandler dst, Vector3 wallDim)
    {
        roomTileset = newRoomTileset;
        owner = generator;
        roomPart = (isFloor) ? eROOM_PART.Floor : eROOM_PART.Roof;
        
        //tiles = new List<GameObject>();
        roomCenter = center;
            
        //Surface tile dimensions
        dimensions = Utils.instance.GetTileDimensions(roomTileset.GetDefault(roomPart).prefab);
        wallDimensions = wallDim;
        
        if (src.IsValid() && dst.IsValid())
        {          
            srcHandler = src;
            dstHandler = dst;
            
            oppositeDir = (dst.parent.position - src.parent.position).normalized;
            
            roomStart = src.walls[0][0].gameObject.transform.position - src.alignDirection * (wallDimensions.x / 2f) + oppositeDir * (wallDimensions.z/2f);
            roomEnd = dst.walls[0][0].gameObject.transform.position - dst.alignDirection * (wallDimensions.x / 2f) - oppositeDir * (wallDimensions.z/2f);

            parent.transform.position = GetParentPos();
            roomEnd.y = parent.position.y;
            roomStart.y = parent.position.y;

            roomDimensions = roomEnd - roomStart;
            //TODO : FIX ME Considering possible rectangle rooms
            prevTilesPerRow = (tilesPerRow == 0) ? (int)Mathf.Sqrt(parent.childCount) : tilesPerRow;
            tilesPerRow = (int)(Mathf.Abs(roomDimensions.x) / wallDimensions.x);
        } else {
            prevTilesPerRow = (tilesPerRow == 0) ? parent.childCount : tilesPerRow;
            tilesPerRow = 0;
        }
    }

    public Vector3 GetParentPos()
    {
        float yWallSize = wallDimensions.y;
        
        if (isFloor) {
            return new Vector3(parent.transform.position.x, srcHandler.parent.position.y - ((yWallSize * srcHandler.height/2f) + dimensions.y/2f), parent.transform.position.z);
        } else
        {
            return new Vector3(parent.transform.position.x, srcHandler.parent.position.y + ((yWallSize * srcHandler.height / 2f) + (dimensions.y / 2f)), parent.transform.position.z);
        }
    }
    
    public void Generate()
    {
        //Load default tile infos data
        RoomTileset.TileInfo roomTileInfo = roomTileset.GetDefault(roomPart);
        if (tilesPerRow > 0)
        {
            Vector3 startPos = roomStart + oppositeDir * (dimensions.z/2f) + srcHandler.alignDirection * (dimensions.x/2f);
            Vector3 spawnPos = startPos;
            if (tiles == null || tiles.Count == 0)
            {
                tiles = new List<SurfaceInfos>();
                if (parent.childCount > 0)
                {
                    Rebind();
                } else {
                    for (int i = 0; i < tilesPerRow; ++i)
                    {
                        for (int j = 0; j < tilesPerRow; ++j)
                        {
                            GameObject newTile = null;
                            #if UNITY_EDITOR
                            newTile = (GameObject)PrefabUtility.InstantiatePrefab(roomTileInfo.prefab, parent);
                            #else
                            newTile = GameObject.Instantiate(roomTileInfo.prefab, parent);
                            #endif
                            newTile.transform.position = spawnPos;
                            newTile.transform.rotation = parent.rotation;
            
                            tiles.Add(new SurfaceInfos(newTile.gameObject, owner, this));
            
                            spawnPos += new Vector3(dimensions.x, 0f, 0f);
                        }
                        spawnPos = new Vector3(startPos.x, startPos.y, spawnPos.z - dimensions.x);
                    }
                }
            }else {
                RegenerateSurface();
            }
        } else {
            Destroy();
        }

        initialized = true;
    }

    public void Rebind()
    {
        //Debug.Log("rebind");
        tiles = new List<SurfaceInfos>();
        //REBIND REFERENCES
        for (int i = 0; i < parent.childCount; ++i)
        {
            Surface surfaceComp = parent.GetChild(i).GetComponent<Surface>();
                
            if(surfaceComp.typeName == "")
                surfaceComp.Autoname();
                
            tiles.Add(new SurfaceInfos(surfaceComp.gameObject, owner, this));
        }
    }

    public void RegenerateSurface()
    {
        Vector3 startPos = roomStart + oppositeDir * (dimensions.z/2f) + srcHandler.alignDirection * (dimensions.x/2f);
        Vector3 spawnPos = startPos;
        int count = 0;
        
        if (tilesPerRow > prevTilesPerRow)
        {
            //Load default tile infos data
            RoomTileset.TileInfo roomTileInfo = roomTileset.GetDefault(roomPart);
            //Debug.Log("Regenerating surface for more");
            for (int i = 0; i < tilesPerRow*tilesPerRow; ++i)
            {
                if (count < prevTilesPerRow*prevTilesPerRow)
                {
                    tiles[count].gameObject.transform.position = spawnPos;
                }else {
                    GameObject newTile = null;
                    #if UNITY_EDITOR
                        newTile = (GameObject)PrefabUtility.InstantiatePrefab(roomTileInfo.prefab, parent);
                    #endif
                    if(!newTile)
                        newTile = GameObject.Instantiate(roomTileInfo.prefab, parent);
                    newTile.transform.position = spawnPos;
                    newTile.transform.rotation = parent.rotation;
            
                    tiles.Add(new SurfaceInfos(newTile.gameObject, owner, this));
                }
                
                spawnPos += new Vector3(dimensions.x, 0f, 0f);
                ++count;
                
                if ((i+1) % tilesPerRow == 0)
                {
                    spawnPos = new Vector3(startPos.x, startPos.y, spawnPos.z - dimensions.x);
                }
            }
        }else if (tilesPerRow < prevTilesPerRow)
        {
            //Debug.Log("Regenerating surface for less");
            for (int i = 0; i < prevTilesPerRow*prevTilesPerRow; ++i)
            {
                if (count < tilesPerRow*tilesPerRow)
                {
                    tiles[count].gameObject.transform.position = spawnPos;
                }
                else
                {
                    Utils.instance.Destroyasse(tiles[count].gameObject);
                }
        
                spawnPos += new Vector3(dimensions.x, 0f, 0f);
                ++count;
                
                if ((i+1) % tilesPerRow == 0)
                {
                    spawnPos = new Vector3(startPos.x, startPos.y, spawnPos.z - dimensions.x);
                }
            }
                
            while(tiles.Count > tilesPerRow*tilesPerRow)
                tiles.RemoveAt(tiles.Count-1);
        }
    }

    public SurfaceInfos GetSurface(GameObject go)
    {
        if (tiles != null)
        {
            for (int i = 0; i < tiles.Count; ++i)
            {
                if(!tiles[i].gameObject)
                    Debug.Log("Game object null");
                if (tiles[i].gameObject == go)
                    return tiles[i];
            }
        }

        return null;
    }

    public Surface ChangeSurfaceType(SurfaceInfos surface, string newType)
    {
        //Debug.Log(parent);
        GameObject oldGO = surface.gameObject;
        int index = oldGO.transform.GetSiblingIndex();
        GameObject tmp = null;

        if (roomTileset.GetPrefab(newType, out tmp, roomPart))
        {
            GameObject newWallGO = null;
            #if UNITY_EDITOR
                newWallGO = (GameObject)PrefabUtility.InstantiatePrefab(tmp, parent);
            #else
                newWallGO = GameObject.Instantiate(tmp, parent);
            #endif
            Surface surfaceComp = newWallGO.GetComponent<Surface>();
            
            newWallGO.transform.position = oldGO.transform.position;
            newWallGO.transform.localRotation = tmp.gameObject.transform.rotation;

            newWallGO.transform.SetSiblingIndex(index);
            
            Utils.instance.Destroyasse(oldGO);
            
            surface.Reset(newWallGO, owner, this);
            
            return surfaceComp;
        }

        return null;
    }
    
    public void RefreshSurfaces()
    {
        Rebind();
        foreach (SurfaceInfos surfaceInfos in tiles)
        {
            //Finding the appropriate preset prefab for the current surface
            GameObject preset = roomTileset.PickTileset(roomPart).Find(x => x.prefab.GetComponent<Surface>().typeName == surfaceInfos.gameObject.GetComponent<Surface>().typeName).prefab;
            //Checking if materials of the preset are the same than the instance game object
            RefreshSurface(surfaceInfos.gameObject, preset);
        }
    }

    private void RefreshSurface(GameObject instance, GameObject prefabPreset)
    {
        //Comparing Game objects mesh renderers count for materials
        Surface instSurfaceComp = instance.GetComponent<Surface>();
        Surface presSurfacecomp = prefabPreset.GetComponent<Surface>();

        if (instSurfaceComp.renderers.Count == presSurfacecomp.renderers.Count) {
            for(int renderer = 0; renderer < instSurfaceComp.renderers.Count; ++renderer)
            {
                MeshRenderer currInstRender = instSurfaceComp.renderers[renderer];
                MeshRenderer currPresRender = presSurfacecomp.renderers[renderer];
                //Iterating over each material in the current mesh renderer
                for(int mat = 0; mat < currInstRender.sharedMaterials.Length; ++mat)
                {
                    Material[] newMats = currInstRender.sharedMaterials;
                    if (newMats[mat] != currPresRender.sharedMaterials[mat])
                    {
                        Debug.Log("No correct material, resetting, old one was " + currInstRender.sharedMaterials[mat].name + " new is " + currPresRender.sharedMaterials[mat].name);
                        //Setting the material as the one in the preset prefab
                        newMats[mat] = currPresRender.sharedMaterials[mat];
                    }

                    currInstRender.sharedMaterials = newMats;
                }
            }
        }
    }
    
    public void Destroy()
    {
        if (tiles != null)
        {
            for (int i = 0; i < tiles.Count; ++i)
            {
                if(tiles[i].gameObject)
                    Utils.instance.Destroyasse(tiles[i].gameObject);
            }
            tiles.Clear();
        }
    }

    public bool IsValid()
    {
        return tiles != null && tiles.Count == parent.childCount && !tiles.Any(x => x.gameObject == null);
    }
}
