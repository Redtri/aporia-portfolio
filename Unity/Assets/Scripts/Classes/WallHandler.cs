using System;
using System.Collections;
#if UNITY_EDITOR
using UnityEditor;
#endif
using UnityEngine;
using System.Collections.Generic;
using UnityEngine.Rendering.UI;
using System.Collections.ObjectModel;

[System.Serializable]
public class WallHandler
{
    [HideInInspector] public RoomTileset roomTileset;

    [System.Serializable]
    public class WallInfos
    {
        public GameObject gameObject;
        public int floor;

        public WallInfos(GameObject go, int wallFloor, RoomGenerator generator, WallHandler handler)
        {
            gameObject = go;
            floor = wallFloor;
            gameObject.GetComponent<Wall>().floor = floor;
            gameObject.GetComponent<Wall>().generator = generator;
            gameObject.GetComponent<Wall>().handler = handler;
        }

        public void Reset(GameObject go, int wallFloor, RoomGenerator generator, WallHandler handler)
        {
            gameObject = go;
            floor = wallFloor;
            gameObject.GetComponent<Wall>().floor = floor;
            gameObject.GetComponent<Wall>().generator = generator;
            gameObject.GetComponent<Wall>().handler = handler;
        }
    }

    public enum eCAPE
    {
        NORTH,
        EAST,
        SOUTH,
        WEST
    }

    public eCAPE cape;
    public Transform parent;

    private Transform roomCenter;

    //Initial amount of walls
    [HideInInspector] public int nbWalls;
    [HideInInspector] public int height;

    [HideInInspector] public int prevNbWalls;

    [HideInInspector] public int prevHeight;

    //Row direction in which walls are cloned
    [HideInInspector] public Vector3 alignDirection;
    [HideInInspector] public Vector3 wallDimensions;
    [HideInInspector] public Vector3 startOffset;
    public List<Transform> floorParents;
    public List<List<WallInfos>> walls;
    private RoomGenerator owner;

    [HideInInspector] public bool initialized = false;

    public void Init(RoomGenerator generator, RoomTileset newRoomTileset, Transform center, int startNbWalls, int newHeight)
    {
        owner = generator;
        wallDimensions = Utils.instance.GetTileDimensions(newRoomTileset.GetDefault(eROOM_PART.Wall).prefab);
        roomTileset = newRoomTileset;
        alignDirection = parent.transform.right;
        roomCenter = center;
        prevHeight = (height > 0) ? height : newHeight;
        height = newHeight;
        prevNbWalls = (startNbWalls > 0) ? nbWalls : startNbWalls;
        nbWalls = startNbWalls;
    }

    public void GenerateWalls(bool forceRegenerate = false)
    {
        //Load default wall infos data
        RoomTileset.TileInfo roomTileInfo = roomTileset.GetDefault(eROOM_PART.Wall);
        //Setting the cape infos : North, West, East, South, with correct offset for wall parents
        SetCape(nbWalls);
        
        //Setting left start position from parent centered position
        Vector3 startPosition = parent.position - (alignDirection * ((nbWalls/2f) * wallDimensions.x + wallDimensions.x/2f));

        //Checking if we're not destroying the room
        if (nbWalls > 0) {
            if (walls == null || walls.Count == 0)
            {
                floorParents = new List<Transform>();
                walls = new List<List<WallInfos>>();
                Vector3 firstFloorPos = parent.position + (-parent.up) * (wallDimensions.y * (height/2f) - wallDimensions.y/2f);

                for (int floor = 0; floor < height; ++floor)
                {
                    //Creating a parent containing all the walls of a specific floor
                    GameObject newFloor = new GameObject();
                    newFloor.transform.parent = parent;
                    newFloor.name = "Floor_" + floor;
                    newFloor.tag = "Wall";
                    newFloor.transform.rotation = parent.rotation;
                    newFloor.transform.position = firstFloorPos + parent.up * wallDimensions.y * floor;
                    //Adding it to the floor parents list 
                    floorParents.Add(newFloor.transform);
                    //Creating a new list of walls for this floor
                    walls.Add(new List<WallInfos>());
                    //Resetting startPosition considering the current floor height
                    startPosition = newFloor.transform.position - (alignDirection * ((nbWalls / 2f) * wallDimensions.x + wallDimensions.x / 2f));
                    
                    for (int wall = 0; wall < nbWalls; ++wall)
                    {
                        //Instantiating prefabs for prefab dependencies parented by the newFloor object

                        GameObject newWallGO = null;
                        
                        #if UNITY_EDITOR
                        newWallGO = (GameObject)PrefabUtility.InstantiatePrefab(roomTileInfo.prefab, newFloor.transform);
                        #else
                        newWallGO = GameObject.Instantiate(roomTileInfo.prefab, newFloor.transform);
                        #endif
                        //Setting its transform informations
                        newWallGO.transform.position = startPosition + alignDirection * wallDimensions.x;
                        newWallGO.transform.rotation = parent.rotation;
                        walls[floor].Add(new WallInfos(newWallGO, floor, owner, this));

                        startPosition = newWallGO.transform.position;
                    }
                }
            }
            else
                RegenerateWalls();
        } else {
            Destroy();
        }

        initialized = true;
    }

    private void RegenerateWalls()
    {
        //Debug.Log("Walls already existing, regenerating");

        if(nbWalls != prevNbWalls)
            RegenerateForWidth();
        else if(height != prevHeight)
            RegenerateForHeight();
    }

    public void RegenerateForHeight()
    {
        float wallFullSize = GetFullWallSize();
        //Add floor
        if (height > prevHeight)
        {
            Vector3 firstFloorPos = parent.position + (-parent.up) * (wallDimensions.y * (height/2f) - wallDimensions.y/2f);
            //Load default wall infos data
            RoomTileset.TileInfo roomTileInfo = roomTileset.GetDefault(eROOM_PART.Wall);
            
            for (int floor = 0; floor < height; ++floor)
            {
                //Moving old floors to be centered with room center
                if (floor < prevHeight)
                {
                    floorParents[floor].position = firstFloorPos + parent.up * floor * wallDimensions.y;
                }//Adding new floors
                else {
                    //Creating a parent containing all the walls of the new floor
                    GameObject newFloor = new GameObject();
                    newFloor.transform.parent = parent;
                    newFloor.name = "Floor_" + floor;
                    newFloor.tag = "Wall";
                    newFloor.transform.rotation = parent.rotation;
                    newFloor.transform.position = firstFloorPos + parent.up * floor * wallDimensions.y;
                    //Adding it to the floor parents list 
                    floorParents.Add(newFloor.transform);
                    //Creating a new list of walls for this floor
                    walls.Add(new List<WallInfos>());
                
                    Transform floorParent = floorParents[floor];
                
                    //Resetting startPosition considering the current floor height
                    Vector3 startPos = floorParent.position - (alignDirection * (wallFullSize / 2f + wallDimensions.x / 2f));

                    for (int wall = 0; wall < nbWalls; ++wall)
                    {
                        GameObject newWallGO = null;
                        //Instantiating prefabs for prefab dependencies parented by the newFloor object
                        #if UNITY_EDITOR
                            newWallGO = (GameObject)PrefabUtility.InstantiatePrefab(roomTileInfo.prefab, newFloor.transform);
                        #endif
                        if(!newWallGO)
                            newWallGO = GameObject.Instantiate(roomTileInfo.prefab, newFloor.transform);
                        //Setting its transform informations
                        newWallGO.transform.position = startPos + alignDirection * wallDimensions.x;
                        newWallGO.transform.rotation = parent.rotation;
                        walls[floor].Add(new WallInfos(newWallGO, floor, owner, this));

                        startPos = newWallGO.transform.position;
                    }
                }
            }
        }//Remove floor
        else if (height < prevHeight)
        {
            Vector3 firstFloorPos = parent.position + (-parent.up) * (wallDimensions.y * (height/2f) - wallDimensions.y/2f);
            
            for (int floor = 0; floor < prevHeight; ++floor)
            {
                List<WallInfos> currFloor = walls[floor];
                Transform floorParent = floorParents[floor];

                //Moving old floors to be centered with room center
                if (floor < height)
                {
                    floorParent.position = firstFloorPos + parent.up * floor * wallDimensions.y;
                }//Removing extra floors
                else
                {
                    Utils.instance.Destroyasse(floorParent.gameObject);

                    while (currFloor.Count > height)
                        currFloor.RemoveAt(currFloor.Count-1);
                }
            }

            while (floorParents.Count > height)
            {
                floorParents.RemoveAt(floorParents.Count-1);
                walls.RemoveAt(walls.Count-1);
            }
        }
    }

    public void RegenerateForWidth()
    {
        float wallFullSize = GetFullWallSize();
        //Add walls
        if (nbWalls > prevNbWalls)
        {
            for (int floor = 0; floor < height; ++floor)
            {
                Transform floorParent = floorParents[floor];

                Vector3 startPos = floorParent.position - (alignDirection * (wallFullSize / 2f + wallDimensions.x / 2f));

                for (int wall = 0; wall < nbWalls; ++wall)
                {
                    //Moving old walls
                    if (wall < prevNbWalls)
                    {
                        WallInfos currWall = walls[floor][wall];
                        currWall.gameObject.transform.position = startPos + alignDirection * wallDimensions.x;
                        //Debug.Log("For more. Existing, moving it : " + wall + ". Floor height : " + floor + ". Total height : " + height + ". " + currWall.gameObject.transform.localPosition);
                        startPos = floorParent.GetChild(wall).position;
                    } //Adding new walls
                    else
                    {
                        //Loading new default wall data
                        RoomTileset.TileInfo roomTileInfo = roomTileset.GetDefault(eROOM_PART.Wall);
                        Vector3 tileDimensions = Utils.instance.GetTileDimensions(roomTileInfo.prefab);
                        GameObject newWallGO = null;
                        //Instantiating wall prefab and setting its transform attributes
                        #if UNITY_EDITOR
                        newWallGO = (GameObject) PrefabUtility.InstantiatePrefab(roomTileInfo.prefab, parent);
                        #else
                        newWallGO = GameObject.Instantiate(roomTileInfo.prefab, parent);   
                        #endif
                        newWallGO.transform.position = startPos + alignDirection * tileDimensions.x;
                        newWallGO.transform.rotation = floorParent.rotation;
                        newWallGO.transform.parent = floorParent;

                        walls[floor].Add(new WallInfos(newWallGO, floor, owner, this));
                        //Debug.Log("For more. Adding : " + wall + ". Floor height : " + floor + ". Total height : " + height + ". " + newWallGO.transform.localPosition);
                        startPos = newWallGO.transform.position;
                    }
                }
            }
        } else if (nbWalls < prevNbWalls) {
            for (int floor = 0; floor < height; ++floor)
            {
                Transform floorParent = floorParents[floor];
                List<WallInfos> currFloor = walls[floor];

                Vector3 startPos = floorParent.position - (alignDirection * (wallFullSize / 2f + wallDimensions.x / 2f));

                for (int wall = 0; wall < prevNbWalls; ++wall)
                {
                    //Moving old walls
                    if (wall < nbWalls) {
                        WallInfos currWall = currFloor[wall];
                        currWall.gameObject.transform.position = startPos + alignDirection * wallDimensions.x;
                        //Debug.Log("For less. Existing, moving it : " + wall + ". Floor height : " + floor + ". Total height : " + height + ". " + currWall.gameObject.transform.localPosition);
                        startPos = floorParent.GetChild(wall).position;
                    } //Removing walls
                    else {
                        Utils.instance.Destroyasse(currFloor[wall].gameObject);
                    }
                }
                while(currFloor.Count > nbWalls)
                    currFloor.RemoveAt(currFloor.Count - 1);
            }
        }
    }
    
    private void SetCape(int newNbWalls)
    {
        switch (cape)
        {
            case eCAPE.EAST:
                startOffset = roomCenter.right; 
                break;
            case eCAPE.WEST:
                startOffset = -roomCenter.right;
                break;
            case eCAPE.NORTH:
                startOffset = roomCenter.forward;
                break;
            case eCAPE.SOUTH:
                startOffset = -roomCenter.forward;
                break;
        }
        startOffset *= (GetFullWallSize()/2f) + Utils.instance.GetTileDimensions(roomTileset.GetDefault(eROOM_PART.Wall).prefab).z/2f;
        startOffset.y = 0f;
        parent.position = roomCenter.position + startOffset;
    }
    public void Destroy()
    {
        //Debug.Log("Destroying handler content");
        if (walls != null)
        {
            while(walls.Count > 0)
            {
                List<WallInfos> currFloor = walls[walls.Count-1];
                Transform floorParent = floorParents[walls.Count-1];

                while (currFloor.Count > 0)
                {
                    Utils.instance.Destroyasse(currFloor[currFloor.Count-1].gameObject);
                    currFloor.RemoveAt(currFloor.Count - 1);
                }
                Utils.instance.Destroyasse(floorParent.gameObject);
                currFloor.Clear();
                walls.Remove(currFloor);
            }
            walls.Clear();
        }
    }

    public bool Contains(GameObject go)
    {
        for (int i = 0; i < walls.Count; ++i)
        {
            for (int j = 0; j < walls[i].Count; ++j)
            {
                if (walls[i][j].gameObject == go)
                    return true;
            }
        }
        return false;
    }

    public WallInfos GetWall(GameObject go)
    {
        if (walls != null)
        {
            for (int i = 0; i < walls.Count; ++i)
            {
                for (int j = 0; j < walls[i].Count; ++j)
                {
                    if(!walls[i][j].gameObject)
                        Debug.Log("Game object null");
                    if (walls[i][j].gameObject == go)
                        return walls[i][j];
                }
            }
        }

        return null;
    }

    public float GetFullWallSize()
    {
        return (wallDimensions.x) * nbWalls;
    }

    public Wall ChangeWallType(WallInfos wall, string newType)
    {
        //Debug.Log(parent);
        GameObject oldGO = wall.gameObject;
        int index = oldGO.transform.GetSiblingIndex();
        GameObject tmp = null;

        if (roomTileset.GetPrefab(newType, out tmp, eROOM_PART.Wall))
        {
            GameObject newWallGO = null;
            #if UNITY_EDITOR
            newWallGO = (GameObject)PrefabUtility.InstantiatePrefab(tmp, parent);
            #else
            newWallGO = GameObject.Instantiate(tmp, parent);
            #endif
            Wall wallComp = newWallGO.GetComponent<Wall>();
            
            newWallGO.transform.position = oldGO.transform.position;
            newWallGO.transform.localRotation = tmp.gameObject.transform.rotation;

            newWallGO.transform.parent = floorParents[wall.floor];
            newWallGO.transform.SetSiblingIndex(index);
            
            Utils.instance.Destroyasse(oldGO);
            
            wall.Reset(newWallGO, wall.floor, owner, this);
            
            return wallComp;
        }

        return null;
    }

    public bool CanRebind()
    {
        return parent.childCount > 0;
    }
    
    public void RebindWalls(out bool obsolete)
    {
        walls = new List<List<WallHandler.WallInfos>>();
        floorParents = new List<Transform>();
        List<GameObject> duplicatas = new List<GameObject>();
        
        bool isRoomObsolete = true, duplicata = false;
        
        //First, checking if there are "Floor" container objects. Handling old tool versions
        //Debug.Log(owner + " " + parent);
        for (int i = 0; i < parent.childCount; ++i)
        {
            //The room is declared obsolete if wall prefabs have been instantiated without "Floor" object parent containers
            if (parent.GetChild(i).name.Contains("Floor")) {
                //Debug.Log("Incorrect child detected : " + parent.GetChild(i).name + " has " + parent.GetChild(i).GetComponent<Wall>());
                isRoomObsolete = false;
                break;
            }
        }

        if (isRoomObsolete) {
            prevHeight = height = 1;
            //Debug.Log(parent.name + " wall of " + parent.parent.parent.name + " room is obsolete, updating");
            //Obsolete rooms do not have more than 1 floor
            Vector3 firstFloorPos = parent.position + (-parent.up) * (wallDimensions.y * (height/2f) - wallDimensions.y/2f);

            //Creating a parent containing all the walls of a specific floor
            GameObject newFloor = new GameObject();
            newFloor.name = "Floor_0";
            newFloor.tag = "Wall";
            newFloor.transform.parent = null;
            newFloor.transform.rotation = parent.rotation;
            newFloor.transform.position = firstFloorPos;
            //Adding it to the floor parents list 
            floorParents.Add(newFloor.transform);
            //Creating a new list of walls for this floor
            List<WallInfos> newWall = new List<WallInfos>();
            walls.Add(newWall);

            int increment = 0, maxOverflow = parent.childCount;
            //Counting the amount of walls inside the parent
            while (increment < maxOverflow)
            {
                Wall wallComp = parent.GetChild(0).GetComponent<Wall>();
                wallComp.transform.parent = newFloor.transform;
                //wallComp.transform.localPosition = new Vector3(wallComp.transform.localPosition.x, 0f, wallComp.transform.localPosition.z);
                
                newWall.Add(new WallInfos(wallComp.gameObject, wallComp.floor, owner, this));
                increment++;
            }
            newFloor.transform.parent = parent;
        } else {
            //Debug.Log(parent.name + " wall of " + parent.parent.parent.name + " room  is up-to-date, rebinding. WITH CHILDCOUNT : " + parent.childCount);
            //Basic values have been initialized previously in the Init function, such as height, nbWalls, and so on.. we can use them to predict child informations

            for (int floor = 0; floor < height; ++floor)
            {
                Transform floorParent = parent.GetChild(floor);
                //Creating a new wall container as the new floor
                walls.Add(new List<WallInfos>());
                floorParents.Add(floorParent);
                
                for (int wall = 0; wall < nbWalls; ++wall)
                {
                    Wall wallComp = floorParent.GetChild(wall).GetComponent<Wall>();
                    
                    walls[floor].Add(new WallInfos(wallComp.gameObject, wallComp.floor, owner, this));
                    
                    if(wallComp.typeName == "")
                        wallComp.Autoname();
                    
                }
            }
        }

        obsolete = isRoomObsolete;
    }

    public void RefreshWalls()
    {
        bool obsolete;
        if(CanRebind())
            RebindWalls(out obsolete);
        foreach (List<WallInfos> floor in walls)
        {
            foreach (WallInfos wallInfos in floor)
            {
                //Finding the appropriate preset prefab for the current wall
                GameObject preset = roomTileset.wallTileset.Find(x => wallInfos.gameObject.GetComponent<Wall>().typeName == x.prefab.GetComponent<Wall>().typeName).prefab;
                //Checking if materials of the preset are the same than the instance game object
                RefreshWall(wallInfos.gameObject, preset);
            }
        }
    }

    private void RefreshWall(GameObject instance, GameObject prefabPreset)
    {
        //Comparing Game objects mesh renderers count for materials
        Wall instWallComp = instance.GetComponent<Wall>();
        Wall presWallcomp = prefabPreset.GetComponent<Wall>();

        Debug.Log("Tyring to refresh wall " + instWallComp.name + " with " + presWallcomp.name);
        if (instWallComp.renderers.Count == presWallcomp.renderers.Count) {
            
            Debug.Log("Equals");
            for(int renderer = 0; renderer < instWallComp.renderers.Count; ++renderer)
            {
                MeshRenderer currInstRender = instWallComp.renderers[renderer];
                MeshRenderer currPresRender = presWallcomp.renderers[renderer];
                //Iterating over each material in the current mesh renderer
                for(int mat = 0; mat < currInstRender.sharedMaterials.Length; ++mat)
                {
                    Material[] newMats = currInstRender.sharedMaterials;
                    if (newMats[mat] != currPresRender.sharedMaterials[mat])
                    {
                        Debug.Log("Not correct mat, resetting " + currInstRender.sharedMaterials[mat].name + " but " + currPresRender.sharedMaterials[mat].name);
                        //Setting the material as the one in the preset prefab
                        newMats[mat] = currPresRender.sharedMaterials[mat];
                    }

                    currInstRender.sharedMaterials = newMats;
                }
            }
        }
        else
        {
            
            Debug.Log("not equals");
        }
    }

    public bool IsValid()
    {
        return walls != null && walls.Count > 0;
    }
}
