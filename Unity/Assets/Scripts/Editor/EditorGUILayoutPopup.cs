﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEditor;

[InitializeOnLoad]
public class EditorGUILayoutPopup  : Editor
{
    private static GenericMenu menu;
    private static RoomGenerator generator;
    private static GameObject aimed;
    
    static EditorGUILayoutPopup()
    {
        SceneView.duringSceneGui += OnSceneGUI;
    }

    private static void OnSceneGUI(SceneView view)
    {
        Event e = Event.current;
        if (e.type == EventType.MouseDown && e.button == 1)
        {
            Vector3 mousePos = e.mousePosition;
            float ppp = EditorGUIUtility.pixelsPerPoint;
            mousePos.y = view.camera.pixelHeight - mousePos.y * ppp;
            mousePos.x *= ppp;
 
            Ray ray = view.camera.ScreenPointToRay(mousePos);
            RaycastHit hit;
 
            if (Physics.Raycast(ray, out hit))
            {
                if (Selection.activeTransform == hit.transform)
                {
                    Transform parent = hit.collider.transform.parent;
                    if (parent != null)
                    {
                        switch (parent.tag)
                        {
                            case "Roof":
                                if ((generator = hit.collider.gameObject.GetComponentInParent<RoomGenerator>()) != null)
                                {
                                    menu = new GenericMenu();
                                    for (int i = 0; i < generator.roomTileset.roofTileset.Count; ++i)
                                    {
                                        menu.AddItem(new GUIContent(generator.roomTileset.roofTileset[i].prefab.GetComponent<Surface>().typeName), false,  obj => Callback(obj, eROOM_PART.Roof), i);
                                    }
                                    aimed = hit.collider.gameObject;
                                }
                                menu.ShowAsContext();
                                break;
                            case "Floor":
                                if ((generator = hit.collider.gameObject.GetComponentInParent<RoomGenerator>()) != null)
                                {
                                    menu = new GenericMenu();
                                    for (int i = 0; i < generator.roomTileset.floorTileset.Count; ++i)
                                    {
                                        menu.AddItem(new GUIContent(generator.roomTileset.floorTileset[i].prefab.GetComponent<Surface>().typeName), false, obj => Callback(obj, eROOM_PART.Floor), i);
                                    }
                                    aimed = hit.collider.gameObject;
                                }
                                menu.ShowAsContext();
                                break;
                            case "Wall":
                                if ((generator = hit.collider.gameObject.GetComponentInParent<RoomGenerator>()) != null)
                                {
                                    menu = new GenericMenu();
                                    for (int i = 0; i < generator.roomTileset.wallTileset.Count; ++i)
                                    {
                                        menu.AddItem(new GUIContent(generator.roomTileset.wallTileset[i].prefab.GetComponent<Wall>().typeName), false,  obj => Callback(obj, eROOM_PART.Wall), i);
                                    }
                                    aimed = hit.collider.gameObject;
                                }
                                menu.ShowAsContext();
                                break;
                            default:
                                break;
                        }
                    }
                }
            }
            //e.Use();
        }
    }
    
    static void Callback (object obj, eROOM_PART roomPart)
    {
        int selected = (int)obj;
        List<RoomTileset.TileInfo> tileInfos = generator.roomTileset.PickTileset(roomPart);
        string type = "";

        switch (roomPart)
        {
            case eROOM_PART.Wall:
                type = tileInfos[selected].prefab.GetComponent<Wall>().typeName;
                List<WallHandler> wallHandlers = new List<WallHandler>{generator.nort, generator.east, generator.south, generator.west};

                if (wallHandlers.Any(x => x.parent == aimed.transform.parent.parent))
                {
                    WallHandler handler = generator.GetHandler(aimed.transform.parent.parent);

                    WallHandler.WallInfos infos = handler.GetWall(aimed);
                    if (infos != null) {
                        handler.ChangeWallType(infos,type);
                    } else {
                        Debug.LogWarning("Couldn't find the wall aimed");  
                    }
                }
                break;
            case eROOM_PART.Floor:
                type = tileInfos[selected].prefab.GetComponent<Surface>().typeName;
                if (generator.floorHandler.parent == aimed.transform.parent)
                {
                    //Debug.Log("Matching surface parents");
                    SurfaceHandler.SurfaceInfos infos = generator.floorHandler.GetSurface(aimed);
                    if (infos != null) {
                        generator.floorHandler.ChangeSurfaceType(infos,type);
                    } else {
                        Debug.LogWarning("Couldn't find the surface aimed");
                    }
                }
                break;
            case eROOM_PART.Roof:
                type = tileInfos[selected].prefab.GetComponent<Surface>().typeName;
                if (generator.roofHandler.parent == aimed.transform.parent)
                {
                    SurfaceHandler.SurfaceInfos infos = generator.roofHandler.GetSurface(aimed);
                    if (infos != null) {
                        generator.roofHandler.ChangeSurfaceType(infos,type);
                    } else {
                        Debug.LogWarning("Couldn't find the surface aimed");
                    }
                }
                break;
        }
    }
}
