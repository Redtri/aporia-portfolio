﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

[CustomEditor(typeof(RoomGenerator))]
public class EditorRoomGenerator : Editor
{
    private RoomGenerator room;

    private void OnEnable()
    {
    }
    
    private void OnDisable()
    {
    }
    
    public override void OnInspectorGUI()
    {
        room = (RoomGenerator) target;

        DrawDefaultInspector();
        if(GUILayout.Button("Clear artefacts"))
        {
            room.Clear();
        }
        if(GUILayout.Button("Rebind"))
        {
            room.Rebind();
        }
        if(GUILayout.Button("Refresh Materials"))
        {
            room.RefreshMaterials();
        }
    }
}
