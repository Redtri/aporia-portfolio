﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using AK.Wwise;
using UnityEngine;
using DG.Tweening;

#if UNITY_EDITOR
using UnityEditor;
#endif

public class SoundManager : MonoSingleton<SoundManager>
{
    public AK.Wwise.Event[] voicesStart;
    public AK.Wwise.Event[] ambiancesStart;
    public AK.Wwise.Event ambianceSAS;
    
    public List<AK.Wwise.State> anguishStates;
    public int startAnguishState;
    public int currAnguishState { get; private set; }
    public List<AK.Wwise.State> phonoStates;
    public int startPhonoState;
    public int currPhonoState { get; private set; }

    public AK.Wwise.Event[] themes;
    public AK.Wwise.RTPC themeRTPC;
    public AK.Wwise.RTPC lightSparkRTPC;
    public AK.Wwise.RTPC shrinkRTPC;

    public AK.Wwise.Event lightDown;
    public AK.Wwise.Event lightShutDown;
    public AK.Wwise.Event lightBuzz;
    public AK.Wwise.Event cutGlitchEffect;

    public AK.Wwise.Event doorWhisper;

    public void Init()
    {
        TryAmbiances();
    }

    public bool EventPlaying(uint eventID, GameObject go)
    {
        uint count = 15;
        uint[] playingIds = new uint[count];

        AKRESULT result = AkSoundEngine.GetPlayingIDsFromGameObject(go, ref count, playingIds);

        for (int i = 0; i < count; i++)
        {
            uint playingId = playingIds[i];
            uint eventId = AkSoundEngine.GetEventIDFromPlayingID(playingId);

            if (eventId == eventID)
                return true;
        }
        return false;
    }

	public AK.Wwise.Event slideBegin;


    public void StartSlide()
    {
    }

    public void EndSlide()
    {
    }
    

 public AK.Wwise.RTPC masterRTPC;

 public void ChangeMasterVolume(float value)
    {
        masterRTPC.SetGlobalValue(value * 100f);
    }

    private bool paused = false;
    public void Pause()
    {
        if (paused)
            AkSoundEngine.WakeupFromSuspend();
        else
            AkSoundEngine.Suspend();
        paused = !paused;
    }

    public void PlayVoice(int index)
    {
        voicesStart[index].Post(gameObject);
    }
    
    public void ResetAmb()
    {
        TryAmbiances();
    }

    public void ShutLights(bool shutdown)
    {
        if (shutdown)
            lightShutDown?.Post(gameObject);
        else
            lightDown?.Post(gameObject);

    }

    public void ShutLights(bool shutdown, GameObject go)
    {
        if (shutdown)
            lightShutDown?.Post(go);
        else
            lightDown?.Post(go);
    }

    public void TryAmbiances()
    {
        
        for (int i = 0; i < ambiancesStart.Length; ++i)
        {
            if(!EventPlaying(ambiancesStart[i].Id, instance.gameObject))
                ambiancesStart[i].Post(gameObject);
        }
    }

    public void StopAmbiances()
    {
        for (int i = 0; i < ambiancesStart.Length; ++i)
        {
            ambiancesStart[i].Stop(gameObject);
        }
    }
    
    public void SetAnguishState(int index)
    {
        currAnguishState = index;
        RefreshAnguish();
    }
    public void SetAnguishStateID(uint id)
    {
        currAnguishState = anguishStates.IndexOf(anguishStates.Find(x => x.Id == id));
        RefreshAnguish();
    }
    public void AddAnguishState()
    {
        currAnguishState = (currAnguishState + 1) % anguishStates.Count;
        RefreshAnguish();
    }
    
    public void RefreshAnguish()
    {
        Debug.Log("<color=cyan> [WWISE] </color> Anguish state level : " + anguishStates[currAnguishState].Name);
        //Debug.Log(anguishStates.Count);
        AkSoundEngine.SetState(anguishStates[currAnguishState].GroupId, anguishStates[currAnguishState].Id);
    }

    public void SetPhonoState(int index)
    {
        currPhonoState = index;
        RefreshPhono();
    }
    public void SetPhonoStateID(uint id)
    {
        currPhonoState = phonoStates.IndexOf(phonoStates.Find(x => x.Id == id));
        RefreshPhono();
    }
    public void AddPhonoState()
    {
        currPhonoState = (currPhonoState + 1) % phonoStates.Count;
        Debug.Log("Incrementing phonograph state value " + currPhonoState);
        RefreshPhono();
    }
    public void RefreshPhono()
    {
        Debug.Log("<color=cyan> [WWISE] </color> Phono state level : " + phonoStates[currPhonoState].Name);
        AkSoundEngine.SetState(phonoStates[currPhonoState].GroupId, phonoStates[currPhonoState].Id);
    }

    public void SetBothState(int index)
    {
        currPhonoState = index;
        currAnguishState = index;
        RefreshPhono();
        RefreshAnguish();
    }

    public void PlayEvent(AK.Wwise.Event tEvent)
    {
        
    }
    
    public void SetTheme(int index)
    {
        themes[index].Post(gameObject);
    }

    public void StopTheme(int index)
    {
        themes[index].Stop(gameObject);
        themeRTPC.SetGlobalValue(0f);
    }

    public void SetShrinkRTPC(float value)
    {
        shrinkRTPC.SetGlobalValue(value);
        //Debug.Log(shrinkRTPC.GetValue(gameObject));
    }

    public void SetThemeRTPC(float value)
    {
        themeRTPC.SetGlobalValue(value);
    }

    public void SetLightSparkRTPC(float value, GameObject go)
    {
        //Debug.Log("Light spark RTPC");
        lightSparkRTPC.SetValue(go, value);
    }

    public void ThemeInterp(float endValue)
    {
        float oldRTPC = themeRTPC.GetGlobalValue();
        
        DOVirtual.Float(oldRTPC, endValue, 5f,(x) => themeRTPC.SetGlobalValue(x));
    }

    //Count represents the number of times the player did enter the SAS
    public void EnterSAS()
    {
        Debug.Log("<color=cyan> [WWISE] </color> SAS Ambiance");
        ambianceSAS.Post(gameObject);
    }

    public void CutGlitch()
    {
        cutGlitchEffect?.Post(gameObject);
    }
    

    // Update is called once per frame
    void Update()
    {
           
    }
}

#if UNITY_EDITOR

[CustomEditor(typeof(SoundManager))]
public class EditorSoundManager : Editor
{
    public override void OnInspectorGUI()
    {
        DrawDefaultInspector();

        SoundManager script = (SoundManager) target;
        if(GUILayout.Button("Anguish"))
        {
            script.RefreshAnguish();
        }
        if(GUILayout.Button("Phono"))
        {
            script.RefreshPhono();
        }
    }
}
#endif