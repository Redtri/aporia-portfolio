﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public enum eLanguage { English, French }

public static class LanguageManager
{
    public static eLanguage stLanguage;

    public static UnityEvent onLanguageUpdate;

    static LanguageManager()
    {
        onLanguageUpdate = new UnityEvent();
    }

    public static void UpdateLanguage(eLanguage newLanguage)
    {
        stLanguage = newLanguage;
        onLanguageUpdate?.Invoke();
    }
}
