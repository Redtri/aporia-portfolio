﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[Serializable]
struct ChunkTarget
{
    public bool enable;
    public Chunk chunk;
}

public class Chunk : MonoBehaviour
{
    public List<Chunk> disableChunks;
    public List<Chunk> enableChunks;
    public List<GameObject> overlappingObjects;

    private BoxCollider bCollider;

    private void Start()
    {
        bCollider = GetComponent<BoxCollider>();
        overlappingObjects = new List<GameObject>();

        if (bCollider)
        {
            Collider[] colliders;
            colliders = Physics.OverlapBox(transform.position, bCollider.size);

            foreach (Collider col in colliders)
            {
                if (!col.name.Contains("Door"))
                {
                    overlappingObjects.Add(col.gameObject);
                }
            }
        }
    }

    public void Activation(bool enable = true)
    {
        foreach (GameObject go in overlappingObjects)
        {
            //Debug.Log("Activation for " + go.name);
            go.SetActive(enable);
        }
    }
    
    private void OnTriggerEnter(Collider other)
    {
        if (other.name == "Pawn")
        {
            foreach (Chunk chunk in disableChunks)
            {
                chunk.Activation(false);
            }

            foreach (Chunk chunk in enableChunks)
            {
                chunk.Activation(true);
            }
        }
    }

    private void OnTriggerExit(Collider other)
    {
        throw new NotImplementedException();
    }
}
