﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using DG.Tweening;
using UnityEngine.UI;
using AporiaMenu;
using UnityEngine.SceneManagement;

namespace AporiaMenu
{
    [System.Serializable]
    public abstract class MenuItem
    {
        protected MenuItemsHandler itemsHandler;
        public double scrollLagOverride {get; protected set;}

        public MenuItem(MenuItemsHandler owner)
        {
            itemsHandler = owner;
        }

        public virtual void Select()
        {
            
        }

        public virtual void Deselect()
        {
            
        }
        
        public virtual void DirectionInput(Vector2 direction)
        {
        
        }

        public virtual void FireInput()
        {
        
        }
    }

    [System.Serializable]
    public class SliderItem : MenuItem
    {
        public Slider slider;
        private bool dragging;
        public SliderItem(MenuItemsHandler owner, Slider sliderObject) : base(owner)
        {
            slider = sliderObject;
            dragging = false;
        }

        public override void Select()
        {
            itemsHandler.eventSystem.SetSelectedGameObject(slider.gameObject);
            slider.transform.DOScale(Vector3.one * 1.1f, 0.5f).SetEase(Ease.InBounce).SetUpdate(true);
        }

        public override void Deselect()
        {
            slider.transform.DOScale(Vector3.one, 0.5f).SetEase(Ease.OutBounce).SetUpdate(true);
        }
        
        public override void DirectionInput(Vector2 direction)
        {
            base.DirectionInput(direction);

            if (Mathf.Abs(direction.x) > 0.5f)
            {
                if (!dragging)
                {
                    dragging = true;
                    slider.GetComponent<EventTrigger>().OnBeginDrag(null);
                }
            
                slider.value += direction.x * 0.01f;
            }
            else
            {
                if (dragging)
                {
                    dragging = false;
                    slider.GetComponent<EventTrigger>().OnEndDrag(null);
                }
            }
        }
    }

    [System.Serializable]
    public class SelectorItem : MenuItem
    {
        public Selector selector;
        private int index;

        public SelectorItem(MenuItemsHandler owner, Selector selectorObject) : base(owner)
        {
            selector = selectorObject;
            scrollLagOverride = 0.5f;
        }

        public override void FireInput()
        {
        }

        public override void Select()
        {
            itemsHandler.eventSystem.SetSelectedGameObject(selector.gameObject);
            selector.transform.DOScale(Vector3.one * 1.1f, 0.5f).SetEase(Ease.InBounce).SetUpdate(true);
        }

        public override void Deselect()
        {
            selector.transform.DOScale(Vector3.one, 0.5f).SetEase(Ease.OutBounce).SetUpdate(true);
        }

        public override void DirectionInput(Vector2 direction)
        {
            base.DirectionInput(direction);

            if (selector.transform.childCount > 0)
            {
                if (Mathf.Abs(direction.x) > 0.8f)
                {
                    if(direction.x > 0)
                    {
                        selector.Forward();
                    }
                    else
                    {
                        selector.Backward();
                    }
                }
            }
        }
    }


    [System.Serializable]
    public class ButtonItem : MenuItem
    {
        public Button button;

        public ButtonItem(MenuItemsHandler owner, Button buttonObject) : base(owner)
        {
            button = buttonObject;
        }

        public override void Select()
        {
            itemsHandler.eventSystem.SetSelectedGameObject(button.gameObject);
            button.transform.DOScale(Vector3.one * 1.1f, 0.5f).SetEase(Ease.InBounce).SetUpdate(true);
        }

        public override void Deselect()
        {
            button.transform.DOScale(Vector3.one, 0.5f).SetEase(Ease.OutBounce).SetUpdate(true);
        }
        
        public override void FireInput()
        {
            base.FireInput();
            button.onClick.Invoke();
        }

    }

    [System.Serializable]
    public class MenuItemsHandler
    {
        [HideInInspector] public EventSystem eventSystem;
        public Transform container;
        private int currentItemIndex;
        [HideInInspector] public List<MenuItem> menuItems;

        public void FireCurrentItem()
        {
            menuItems[currentItemIndex].FireInput();
        }

        public void DirectionCurrentitem(Vector2 direction, double time)
        {
                menuItems[currentItemIndex].DirectionInput(direction);
        }
        
        public void ResetIndex()
        {
            currentItemIndex = 0;
        }
        
        public void AddToIndex(float value)
        {
            if (value < 0)
            {
                menuItems[currentItemIndex].Deselect();
                
                if (currentItemIndex == menuItems.Count - 1)
                    currentItemIndex = 0;
                else
                    ++currentItemIndex;
                
                menuItems[currentItemIndex].Select();
            }else if (value > 0)
            {
                menuItems[currentItemIndex].Deselect();
                
                if (currentItemIndex == 0)
                    currentItemIndex = menuItems.Count - 1;
                else
                    --currentItemIndex;
                
                menuItems[currentItemIndex].Select();
            }
        }

        public int GetCurrentIndex()
        {
            return currentItemIndex;
        }
        
        public void CreateMenuItems()
        {
            menuItems = new List<MenuItem>();

            for (int i = 0; i < container.childCount; ++i)
            {
                Transform currentChild = container.GetChild(i);
                
                Component component = currentChild.GetComponent<Button>();
                
                if (component)
                {
                    menuItems.Add(new ButtonItem(this, (Button)component));
                }
                else if ((component = currentChild.GetComponent<Slider>()))
                {
                    menuItems.Add(new SliderItem(this, (Slider)component));
                }
                else if((component = currentChild.GetComponent<Selector>()))
                {
                    menuItems.Add(new SelectorItem(this, (Selector)component));
                }
            }
        }
    }
}

public class MenuNavigator : MonoBehaviour
{
    public EventSystem eventSystem;
    public PlayerController controller;
    public List<AporiaMenu.MenuItemsHandler> menuItemsHandlers;
    public List<TextLanguageResponsive> texts;
    public List<ImageLanguageResponsive> images;
    public float scrollLag;
    public float scrollThreshold;
    
    private int currentHandler;
    private bool wasInteracting;
    private double scrollTime;

    [HideInInspector]public bool inMenu;

    private void OnEnable()
    {
        LanguageManager.onLanguageUpdate.AddListener(UpdateLanguage);
        StartCoroutine(InMenuCoroutine());
    }

    private void OnDisable()
    {
        LanguageManager.onLanguageUpdate.RemoveListener(UpdateLanguage);
    }

    // Start is called before the first frame update
    void Start()
    {
        if (SceneManager.GetActiveScene().name.Contains("Menu"))
        {
                    inMenu = true;
            StartCoroutine(InMenuCoroutine());
        }
	    LanguageManager.stLanguage = eLanguage.French;
        UpdateLanguage();
        foreach (AporiaMenu.MenuItemsHandler handler in menuItemsHandlers)
        {
            handler.eventSystem = eventSystem;
            handler.CreateMenuItems();
        }
    }

    private IEnumerator InMenuCoroutine()
    {
        while(inMenu)
        {
            if (controller.interactInput && !controller.usingKeyboard)
            {
                if (!wasInteracting)
                {
                    wasInteracting = true;
                    menuItemsHandlers[currentHandler].FireCurrentItem();
                }
            }
            else
            {
                wasInteracting = false;
            }

            if (Mathf.Abs(controller.movementInput.y) > scrollThreshold || Mathf.Abs(controller.movementInput.x) > scrollThreshold)
            {
                if (Time.realtimeSinceStartup - scrollTime > scrollLag)
                {
                    scrollTime = Time.realtimeSinceStartup;

                    if (controller.movementInput.x != 0.0f)
                        menuItemsHandlers[currentHandler].DirectionCurrentitem(controller.movementInput, scrollTime);

                    if (Mathf.Abs(controller.movementInput.y) > scrollThreshold)
                        menuItemsHandlers[currentHandler].AddToIndex(controller.movementInput.y);
                }
            }
            yield return new WaitForEndOfFrame();
        }
        yield return null;
    }
    
    private void FixedUpdate()
    {
        /*
        if (inMenu)
        {
            if (controller.interactInput && !controller.usingKeyboard)
            {
                if (!wasInteracting)
                {
                    wasInteracting = true;
                    menuItemsHandlers[currentHandler].FireCurrentItem();
                }
            }
            else
            {
                wasInteracting = false;
            }

            if (Mathf.Abs(controller.movementInput.y) > scrollThreshold || Mathf.Abs(controller.movementInput.x) > scrollThreshold)
            {
                Debug.Log("Movement menu ");
                if (Time.time - scrollTime > scrollLag)
                {
                    scrollTime = Time.time;

                    if (controller.movementInput.x != 0.0f)
                        menuItemsHandlers[currentHandler].DirectionCurrentitem(controller.movementInput, scrollTime);

                    if (Mathf.Abs(controller.movementInput.y) > scrollThreshold)
                        menuItemsHandlers[currentHandler].AddToIndex(controller.movementInput.y);
                }
            }
        }*/
    }

    public void UpdateLanguage()
    {
        foreach(TextLanguageResponsive text in texts)
        {
            text.SetText((int)LanguageManager.stLanguage);
        }
        foreach(ImageLanguageResponsive img in images)
        {
            img.SetImage((int)LanguageManager.stLanguage);
        }
    }

    public void SetCurrentHandler(int index)
    {
        currentHandler = index;
        menuItemsHandlers[currentHandler].ResetIndex();
    }
}
