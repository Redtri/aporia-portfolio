﻿using System.Collections;
using System.Collections.Generic;
using System.Runtime.InteropServices;
using UnityEngine;

public class Utils : MonoSingleton<Utils>
{
    public void Destroyasse(GameObject go)
    {
        #if UNITY_EDITOR
        if (Application.isEditor)
        {
            UnityEditor.EditorApplication.delayCall += () =>
            {
                DestroyImmediate(go);
            }; 
        }
        #endif
        if(Application.isPlaying)
        {
            GameObject.Destroy(go);
        }
    }

    public Vector3 GetTileDimensions(GameObject go)
    {
        BoxCollider[] colliders = go.GetComponentsInChildren<BoxCollider>();
        
        Vector3 tmpDimensions = Vector3.zero;

        if (colliders != null)
        {
            for (int i = 0; i < colliders.Length; ++i)
            {
                //Not taking into account overlap colliders
                if (!colliders[i].isTrigger && !colliders[i].CompareTag("AvoidDimensions"))
                {
                    Vector3 increasedChildPos = colliders[i].transform.position + MultVector((colliders[i].size),colliders[i].transform.lossyScale);
                    //Calculating the increased position from the collider bounds
                    Vector3 increasedPos = go.transform.position + MultVector(tmpDimensions, go.transform.localScale);
                    //Width
                    if (increasedChildPos.x > increasedPos.x)
                        tmpDimensions.x += increasedChildPos.x-increasedPos.x;
                    //Height
                    if (increasedChildPos.y > increasedPos.y)
                        tmpDimensions.y += increasedChildPos.y-increasedPos.y;
                    //Depth
                    if (increasedChildPos.z > increasedPos.z)
                        tmpDimensions.z = increasedChildPos.z-increasedPos.z;
                }
            }
        }

        return tmpDimensions;
    }

    public Vector3 MultVector(Vector3 a, Vector3 b)
    {
        return new Vector3(a.x*b.x, a.y*b.y, a.z*b.z);
    }
    
    public int LoopIncrement(int value, int min, int max)
    {
        if (value == max)
            return min;
        
        return value + 1;
    }
    
    public int LoopDecrement(int value, int min, int max)
    {
        if (value == min)
            return max;
        
        return value - 1;
    }
}
