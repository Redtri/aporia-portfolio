﻿using System;
using System.Collections;
using System.Collections.Generic;
using DG.Tweening;
using UnityEngine;
using Random = UnityEngine.Random;

public class TestShake : MonoBehaviour
{
    public float duration;
    public AnimationCurve curve;
    private bool shaking;
    public float amount;
    private float shakeTime;
    private Vector3 startPos;

    private void Start()
    {
        startPos = transform.position;
    }

    public void Shake()
    {
        StartCoroutine(ShakeCo());
    }

    private IEnumerator ShakeCo()
    {
        shakeTime = Time.time;
        float percent = (Time.time - shakeTime) / duration;

        while (percent < 1f)
        {
            percent = (Time.time - shakeTime) / duration;
            float multiplier = curve.Evaluate(percent) * amount;

            transform.position = startPos + Random.insideUnitSphere * multiplier * Time.deltaTime;
            yield return new WaitForEndOfFrame();
        }

        yield return null;
    }
}
