﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class Timer : MonoBehaviour
{
    public float duration;
    public UnityEvent onEnd;
    private float startTime;
    private bool started;

    public void StartTimer()
    {
        if (!started)
        {
            startTime = Time.time;
            started = true;
            StartCoroutine(TimerCo());
        }
    }

    private IEnumerator TimerCo()
    {
        yield return new WaitForSeconds(duration);
        /*
        while (started && Time.time - startTime <= duration)
        {
            yield return new WaitForEndOfFrame();
        }*/

        onEnd?.Invoke();
        started = false;
        yield return null;
    }
}
