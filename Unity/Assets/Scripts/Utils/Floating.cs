﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Random = System.Random;

public class Floating : MonoBehaviour
{
    public float speed;

    public float scale;
    public float waitRange;
    private bool floating;
    private float startTime;
    
    private void Start()
    {
        StartCoroutine(FloatCo());
    }
    
    void Update()
    {
        if (floating)
        {
            transform.position += Vector3.up * Mathf.Sin(Time.time - startTime) * speed * Time.deltaTime;
        }
    }

    private IEnumerator FloatCo()
    {
        yield return new WaitForSeconds(UnityEngine.Random.Range(0f, waitRange));
        floating = true;
        startTime = Time.time;
        yield return null;
    }
}
