﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Playables;

public class TimelineHandler : MonoBehaviour
{
    public PlayableDirector playableDirector;

    public void SetTime(float time)
    {
        playableDirector.time = time;
    }

    private void Update()
    {
        //Debug.Log(playableDirector.time);
    }
}
