﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "New Room Infos", menuName = "Scriptables/RoomInfos", order = 1)]
[System.Serializable]
public class RoomInfos : ScriptableObject
{ 
    public List<string> wallTypes;
    public RoomTileset roomTileset;
}
