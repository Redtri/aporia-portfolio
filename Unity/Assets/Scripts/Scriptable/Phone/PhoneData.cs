﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "New Phone Data", menuName = "Scriptables/PhoneData", order = 1)]
public class PhoneData : ScriptableObject
{
    public Sprite speaker;
    public Sprite alarm;
    public Sprite waiting;
    public Sprite inactive;
}
