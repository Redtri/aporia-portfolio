﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "New Player Data", menuName = "Scriptables/PlayerData", order = 1)]
public class PlayerData : ScriptableObject
{
    [System.Serializable]
    public class InputInfos
    {
        public InputInfos kbMouse;
        public InputInfos gamepad;
        public float lookSensitivity;
        public Vector2 clampLook;
        public float moveSpeed;
        public float floatSpeed;
        public float normalInterp;
    }

    public InputInfos keyboard;
    public InputInfos gamepad;
    public bool usingKeyboard;
    
    public float focusRange;
    public float interactRange;
    public float wallWalkRange;
}
