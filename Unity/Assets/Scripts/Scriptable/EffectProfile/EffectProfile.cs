﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum eEFFECT_PROFILE_TYPE { Glitch, Cut, Desat, Blackhole, Destruct, Bloom, Shake, Trap,WallShake }

[CreateAssetMenu(fileName = "New Effect Profile", menuName = "Scriptables/EffectProfile", order = 1)]
public class EffectProfile : ScriptableObject
{
    public eEFFECT_PROFILE_TYPE profileType;
    public float duration;
    public AnimationCurve curve;
    public AnimationCurve curve2;
    public float intensity;
    public float extraParam1;
    public float extraParam2;
    public float extraParam3;
    public int steps;
    public bool additive;
}
