﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public enum eROOM_PART{Floor, Roof, Wall}

[CreateAssetMenu(fileName = "New Room Tileset", menuName = "Scriptables/RoomTileset", order = 1)]
[System.Serializable]
public class RoomTileset : ScriptableObject
{
    [System.Serializable]
    public class TileInfo
    {
        public GameObject prefab;
    }

    public List<TileInfo> wallTileset;
    public List<TileInfo> roofTileset;
    public List<TileInfo> floorTileset;

    public bool GetPrefab(string typeName, out GameObject go, eROOM_PART roomPart)
    {
        //Picking the appropriate tileset considering the room part wanted
        List<TileInfo> tileset = PickTileset(roomPart);
        //Finding the prefab
        for (int i = 0; i < tileset.Count; ++i)
        {
            switch (roomPart)
            {
                case eROOM_PART.Floor:
                case eROOM_PART.Roof:
                    Surface surfaceComp = tileset[i].prefab.GetComponent<Surface>();
                    if (typeName == surfaceComp.typeName) {
                        go = tileset[i].prefab;
                        return true;
                    }
                    break;
                case eROOM_PART.Wall:
                    Wall wallComp = tileset[i].prefab.GetComponent<Wall>();
                    if (typeName == wallComp.typeName) {
                        go = tileset[i].prefab;
                        return true;
                    }
                    break;
            }
        }

        go = null;
        return false;
    }
    public TileInfo GetDefault(eROOM_PART roomPart)
    {
        //Picking the appropriate tileset considering the room part wanted
        List<TileInfo> tileset = PickTileset(roomPart);
        
        return tileset[0];
    }

    public TileInfo GetFromTypename(string typename, eROOM_PART roomPart)
    {
        //Picking the appropriate tileset considering the room part wanted
        List<TileInfo> tileset = PickTileset(roomPart);
        
        for (int i = 0; i < tileset.Count; ++i)
        {
            Wall wallComp = tileset[i].prefab.GetComponent<Wall>();
            if (typename == wallComp.typeName)
                return tileset[i];
        }

        return null;
    }

    public List<TileInfo> PickTileset(eROOM_PART roomPart)
    {
        switch (roomPart) {
            case eROOM_PART.Floor:
                return floorTileset;
            case eROOM_PART.Roof:
                return roofTileset;
            case eROOM_PART.Wall:
                return wallTileset;
        }

        return null;
    }
}
