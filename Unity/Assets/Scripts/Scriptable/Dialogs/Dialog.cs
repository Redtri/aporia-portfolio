﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum eSpeaker { V, KnowledgeUnit, S_C, G_M}

[CreateAssetMenu(fileName = "New Dialog", menuName = "Scriptables/Dialog", order =1)]
public class Dialog : ScriptableObject
{
    public float approxDuration;

    private static Dictionary<int, string> speakers = new Dictionary<int, string>{
        { (int)eSpeaker.V, "V"},
        { (int)eSpeaker.KnowledgeUnit, "Knowledge Unit"},
        { (int)eSpeaker.S_C, "S&C"},
        { (int)eSpeaker.G_M, "G/M"}
    };



    [System.Serializable]
    public class LineInfo
    {
        public float delay;
        public float duration;
        [TextArea] public string text;
    }

    public virtual string GetLine(int index)
    {
        return lineInfos[index].text;
    }

    public List<LineInfo> lineInfos;
    public AK.Wwise.Event wwEvent;
}
