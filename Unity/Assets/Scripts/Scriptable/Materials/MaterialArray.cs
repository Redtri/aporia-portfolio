﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;



[CreateAssetMenu(fileName = "New Material Array", menuName = "Scriptables/Material Array", order = 1)]
public class MaterialArray : ScriptableObject
{
    public Material[] materials;
}
