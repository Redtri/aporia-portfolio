﻿using System;
using System.Collections;
using System.Collections.Generic;
using DG.Tweening;
using UnityEngine;
using UnityEngine.UIElements;
using Image = UnityEngine.UI.Image;
using UnityEngine.Video;

public class ControlCanvas : MonoBehaviour
{
    public List<Image> planes;
    public VideoPlayer videoPlayer;

    private void Start()
    {
        foreach (Image img in planes)
        {
            img.material.SetFloat("_Alpha", 1f);
        }
    }

    IEnumerator PlayVideo()
    {
        videoPlayer.Prepare();
        while (!videoPlayer.isPrepared)
        {
            yield return new WaitForSeconds(1f);
            break;
        }
        videoPlayer.Play();
    }
    public void FadeIn(float duration)
    {
        StartCoroutine(PlayVideo());
        foreach (Image img in planes)
        {
            img.material.SetTexture("_MainTex", videoPlayer.targetTexture);
            img.material.DOFloat(0f, "_Alpha", duration);
        }
    }

}
