﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

[System.Serializable]
public class ImageLanguageResponsive : MonoBehaviour
{
    public Sprite[] values;
    private Image img;

    private void Start()
    {
        img = GetComponent<Image>();
    }

    public void SetImage(int index)
    {
        if(img && index < values.Length)
        {
            img.sprite = values[index];
        }
    }
}
