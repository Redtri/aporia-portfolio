﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

[System.Serializable]
public class TextLanguageResponsive : MonoBehaviour
{
    public string[] values;
    private Text textArea;

    private void Awake()
    {
        textArea = GetComponent<Text>();
    }

    public void SetText(int index)
    {
        if(textArea && index < values.Length)
        {
            textArea.text = values[index];
        }
    }
}
