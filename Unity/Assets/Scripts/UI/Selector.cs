﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public enum eSelectorFunction { Language };

public class Selector : MonoBehaviour
{
    public Button left;
    public Button right;
    public Text textArea;

    public eSelectorFunction selectorFunction;

    public int maxValue;

    public int index { get; private set; } = 0;

    private void Start()
    {
        Refresh();
    }

    public void Backward()
    {
        if (index > 0)
            --index;
        else
            index = maxValue - 1;
        Refresh();
    }

    public void Forward()
    {
        if (index == maxValue - 1)
            index = 0;
        else
            ++index;
        Refresh();
    }

    private void Refresh()
    {
        switch (selectorFunction)
        {
            case eSelectorFunction.Language:
                LanguageManager.UpdateLanguage((eLanguage)index);
                break;
        }
    }
}
