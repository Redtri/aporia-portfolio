﻿using System;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using DG.Tweening;
using UnityEngine.PlayerLoop;

public class FacingTarget : MonoBehaviour
{
    public string targetNameOverride;
    public Transform target;
    public bool lockX;
    public bool lockY;
    public bool lockZ;

    public AnimationCurve distAdaptCurve;
    public float maxDist;
    public float maxSpeed;

    public bool activated;
    public AK.Wwise.Event rotationEvent;
    public bool rotateEventOnLook;
    
    private Vector3 lastTargetPos;
    private bool rotating;

    private void Start()
    {
        if (targetNameOverride != "")
            target = GameObject.Find(targetNameOverride).transform;
    }

    private void Update()
    {
        if(activated)
            LookAtTarget();
    }

    public void Activate()
    {
        if (!activated)
        {
            activated = true;
        }
    }

    public void Deactivate()
    {
        if (activated)
            activated = false;
    }

    private void LookAtTarget()
    {
        float speedMultiplier = maxSpeed;
        if (maxSpeed > 0)
        {
            speedMultiplier = distAdaptCurve.Evaluate(1f-((target.position - transform.position).magnitude / maxDist)) * 
                              maxSpeed;
        }
        Quaternion rot = Quaternion.Slerp(transform.rotation, Quaternion.Euler(lookRotation), speedMultiplier * Time.deltaTime);
        
        if (rotateEventOnLook)
        {
            if (rot != transform.rotation)
            {
                if (!rotating)
                {
                    rotating = true;
                    rotationEvent?.Post(gameObject);
                }
            }else {
                rotationEvent?.Stop(gameObject);
                rotating = false;
            }
        }
        transform.rotation = rot;

        if (lastTargetPos != target.position)
        {
            lastTargetPos = target.position;
            
            if (!rotateEventOnLook)
            {
                if (!rotating)
                {
                    rotating = true;
                    rotationEvent?.Post(gameObject);
                }
            }
        } else {
            if (!rotateEventOnLook)
            {
                rotationEvent?.Stop(gameObject);
                rotating = false;
            }
        }
    }
    
    private Vector3 lookRotation { 
        get{
                Vector3 rotation = Quaternion.LookRotation(target.position - transform.position).eulerAngles;
                if (lockX)
                    rotation.x = 0f;
                if (lockY)
                    rotation.y = 0f;
                if (lockZ)
                    rotation.z = 0f;
                return rotation;
            }
    }
}
