﻿using System;
using System.Collections;
using System.Collections.Generic;
using DG.Tweening;
using UnityEngine;
using UnityEngine.InputSystem;
using UnityEngine.Rendering;
using UnityEngine.Rendering.HighDefinition;
using UnityEngine.SceneManagement;
using UnityEngine.UI;



public class MenuHandler : MonoBehaviour
{
    public MenuNavigator navigator;
    public AK.Wwise.RTPC masterRTPC;
    public AK.Wwise.Event menuEvent;
    public AK.Wwise.Event clicEvent;
    public AK.Wwise.Event slideBegin;
    public Image blackScreen;
    public Transform mainMenu;
    public Transform options;
    public Transform controls;
    public Transform credits;
    private Transform current;
    public Transform camParent;
    public Volume ppVolume;
    public AudioActor phonographe;

    private void Start()
    {
        //AkSoundEngine.StopAll();
        Cursor.visible = true;
        menuEvent?.Post(gameObject);
        current = mainMenu;
        blackScreen.DOFade(0f, 2f).onComplete += () => blackScreen.gameObject.SetActive(false);
        ChangeMasterVolume(.7f);
        DisplayChilds(options, 0.1f, false);
        DisplayChilds(controls, 0.1f, false);
        DisplayChilds(credits, 0.1f, false);
    }

    private void Update()
    {
        Cursor.lockState = CursorLockMode.None;
        Cursor.visible = true;
    }

    public void Play()
    {
        blackScreen.gameObject.SetActive(true);
        blackScreen.DOFade(1f, 2f).onComplete += () => LoadGameScene();
        menuEvent.Stop(gameObject, 3000);
        phonographe.FadeStop();
    }

    public void Menu(bool fromCredits)
    {
        clicEvent?.Post(gameObject);
        navigator.SetCurrentHandler(0);
        
        if (fromCredits)
        {
            camParent.DOMove(new Vector3(-87.3f, 0.015f, -3.213914f), 6f).onComplete += () => DisplayChilds(mainMenu, 1f, true);
            camParent.DORotate(new Vector3(0f, 0f, 0f), 6f);
            DisplayChilds(current, 1f, false);
            current = mainMenu;
        }
        else
        {
            DisplayChilds(current, 1f, false);
            current = mainMenu;
            DisplayChilds(mainMenu, 1f, true);
        }
    }

    public void Options()
    {
        navigator.SetCurrentHandler(1);
        clicEvent?.Post(gameObject);
        DisplayChilds(current, 1f, false);
        current = options;
        DisplayChilds(options, 1f, true);
    }

    public void Controls()
    {
        navigator.SetCurrentHandler(2);
        clicEvent?.Post(gameObject);
        DisplayChilds(current, 1f, false);
        current = controls;
        DisplayChilds(controls, 1f, true);
    }
    
    
    public void Credits()
    {
        navigator.SetCurrentHandler(3);
        clicEvent?.Post(gameObject);
        camParent.DOMove(new Vector3(-89.09f, 0.62f, -4.14f), 6f).onComplete += () => DisplayChilds(credits, 1f, true);
        //camParent.GetComponent<Animator>().SetTrigger("credits");
        camParent.DORotate(new Vector3(-23.968f, -144.651f, -12.463f), 6f);
        DisplayChilds(current, 1f, false);
        current = credits;
    }

    public void StartSlide()
    {
        slideBegin?.Post(gameObject);
    }

    public void EndSlide()
    {
        slideBegin?.Stop(gameObject);
    }
    
    public void Exit()
    {
        clicEvent?.Post(gameObject);
        Application.Quit();
    }

    private void LoadGameScene()
    {
        SceneManager.LoadSceneAsync(1);
    }

    public void ChangeMasterVolume(float value)
    {
        masterRTPC.SetGlobalValue(value * 100f);
    }
    
    public void ChangeLift(float value)
    {
        LiftGammaGain liftGammaGain;
        ppVolume.profile.TryGet(out liftGammaGain);
        if (liftGammaGain)
        {
            liftGammaGain.gain.value = new Vector4(1f, 1f, 1f, value);
        }
    }

    private Sequence DisplayChilds(Transform container, float duration, bool show = true)
    {
        if(show)
            container.gameObject.SetActive(true);
        
        Sequence tmpSequence = DOTween.Sequence();
        foreach (MaskableGraphic mask in container.GetComponentsInChildren<MaskableGraphic>())
        {
            tmpSequence.Join(mask.DOFade(show ? 1f : 0f, duration));
        }

        if (!show)
            tmpSequence.AppendCallback(() => container.gameObject.SetActive(false));

        return tmpSequence;
    }
}
